@echo off
echo Preparing...
rmdir /s /q v2_release_temp >nul 2>&1
echo Building...
mkdir v2_release_temp >nul 2>&1

xcopy v2 v2_release_temp /s /e >nul 2>&1

rmdir /s /q v2_release_temp\cache >nul 2>&1
rmdir /s /q v2_release_temp\testing >nul 2>&1

del v2_release_temp\.buildpath >nul 2>&1
del v2_release_temp\.gitignore >nul 2>&1
del v2_release_temp\.installed >nul 2>&1
del v2_release_temp\.project >nul 2>&1
del v2_release_temp\users.txt >nul 2>&1
del v2_release_temp\config.php >nul 2>&1
del v2_release_temp\public.png >nul 2>&1
del v2_release_temp\public2.png >nul 2>&1
del v2_release_temp\public3.png >nul 2>&1
del v2_release_temp\statistics.src.js >nul 2>&1

cd v2_release_temp >nul 2>&1

"C:\Program Files\7-Zip\7zG.exe" a -tzip v2_release_X.X.X.zip -r * >nul 2>&1

cd .. >nul 2>&1

copy v2_release_temp\v2_release_X.X.X.zip v2_release_X.X.X.zip >nul 2>&1

echo Cleanup...

rmdir /s /q v2_release_temp >nul 2>&1
