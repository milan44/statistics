@echo off
echo Preparing...
rmdir /s /q v2_deploy >nul 2>&1
echo Building...
mkdir v2_deploy >nul 2>&1

xcopy v2 v2_deploy /s /e >nul 2>&1

rmdir /s /q v2_deploy\cache >nul 2>&1
rmdir /s /q v2_deploy\testing >nul 2>&1

del v2_deploy\.buildpath >nul 2>&1
del v2_deploy\.gitignore >nul 2>&1
del v2_deploy\.installed >nul 2>&1
del v2_deploy\.project >nul 2>&1
del v2_deploy\users.txt >nul 2>&1
del v2_deploy\config.php >nul 2>&1
del v2_deploy\public.png >nul 2>&1
del v2_deploy\public2.png >nul 2>&1
del v2_deploy\public3.png >nul 2>&1
del v2_deploy\statistics.src.js >nul 2>&1

echo Press enter when you are finished deploying.

pause >nul 2>&1

echo Removing...

rmdir /s /q v2_deploy >nul 2>&1
