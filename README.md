[![pipeline status](https://gitlab.com/milan44/statistics/badges/master/pipeline.svg)](https://gitlab.com/milan44/statistics/commits/master)

# [Screenshot](screenshot.png)

# What are these fields

|Field in Table|Description|
|--|--|
|Top Sites (yesterday)|Shows the top 3 most clicked pages from yesterday|
|Top Sites (all)|Shows the top 3 most clicked pages from all time|
|--|--|
|Today|How many users/sessions/hits your website got at this current day|
|Average Today|How the average is today|
|Maximum|The maximum users/sessions/hits you got in the last 3 months|
|Tendency|Growth of your site per day (in percent).|
|Bounce Rate|The "Bounce Rate" is calculated by the amount of users who visited the page only once divided by the amount of users who visited the page multiple times|
|User Loyality|Shows how many users that visited your site the day before yesterday, came back yesterday|
|Per second|This shows how many users/sessions/hits your website get per second|
|Average per second|This shows how many average users/sessions/hits your website get per second|
|Average Time a user spends on your site every day (hh:mm:ss)|This shows how long the average user stays on your website per day|

### Activity per hour

Activity per hour shows you the users+hits+sessions per hour (last 3 months)

### Archived Visitors/Sessions/Hits

To save space, every entry that is older than 3 months gets archived (and displayed in this chart).

# How to install

1. Download release of your choice
2. Copy files into directory of your choice
3. Open installer.php in your webbrowser
4. Configure Settings and admin user
5. Add `<script src="path/to/directory/statistics.js"></script>` to the bottom of all pages you want to be tracked.

# [Plugins](PLUGINS.md)

# [Releases](https://gitlab.com/milan44/statistics/blob/release)


### 3.0.0

- Many bugs fixed
- Faster cache reloading
- Better forecasting
- Better security
- Better exporting

### 2.10.0

- Many bugs fixed
- Avg session time is now Average Time a user spends on your site every day (hh:mm:ss)
- fixed time
- automated exporting
- better forecast

### 2.9.5

- Added Average Session Time (mm:ss)
	- This shows how long the average user stays on your website

### 2.9.4

- Better Public statistics

### 2.9.3

- Frontend Bugs fixed
- Added per second
    - This shows how many users/sessions/hits your website get per second
- Added average per second
    - This shows how many average users/sessions/hits your website get per second

### 2.9.2

- Added Auto Backups


### 2.9.1

- Critical Bugs fixed

### 2.9.0

- Added Proxy and Tor detection
    - Now the statistics system will automatically detect users who use tor or a proxy
    - It will generate a .htaccess file which redirects every tor and proxy user to a location of you choice

### 2.8.0

- Added Error prevention
- Made UI more user-friendly
- Changed caching/generation

### 2.7.1

- Fixed hole filler (now if dates are missing they are filled up with 0)
- New Font
- Added Server Timezone

### 2.7.0

- Fixed archiving bug where only hits were archived
- Added AI forecast
- Small style changes

### 2.6.3

- Moved Timezones to extra chart
- Added Device Types

### 2.6.2

- Bug fixing

### 2.6.1

- Bug fixing

### 2.6.0

- Fixed User Loyalty bug
- Added Realtime Pages and Timezones
- Obfuscated statistics.js to prevent spam
- All Javascript gets minified

### 2.5.7

- Added Top Timezones
    - This shows from which Timezones the most users are (in all time and yesterday)
- Moved some frontend stuff in the core plugin

### 2.5.6

- Added additional functionality to the Plugins (see [here](PLUGINS.md))
- Bugs fixed
- Added hole filler (if there is no data for a day, the day will still be show as zero)

### 2.5.5

- Added additional functionality to the PluginHelper (see [here](PLUGINS.md))

### 2.5.4

- Fixed bug where Top Sites (yesterday) would just show nothing
- Logins will now be remembered for 7 days

### 2.5.3

- Added Top Sites
    - This shows which pages on your website are clicked on the most (in all time and yesterday)

### 2.5.0

- Added Realtime
    - Realtime displays how many users are currently on your website (in 2 minutes delay)

### 2.4.0

- Added Plugin functionality (see [here](PLUGINS.md))

### 2.3.6

- User Loyality now shows how many users that visited your site the day before yesterday, came back yesterday
- Tendency was renamed to Average Tendency
- Relative to yesterday was renamed to Tendency
- Tendency now is the difference between the day before yesterday and yesterday
- Average Tendency now is the average difference between the day before yesterday and yesterday
- Fixed Bounce Rate (was always showing 0%)
- Fixed Average Bounce Rate (was always showing 0%)

### 2.3.5

- Fixed multiple bugs
- Added User Loyality
    - User Loyality shows how many users that visited your site yesterday, came back today

### 2.3.1

- Fixed a bug where logs would get archived multiple times

### 2.3.0

- Added Archive Display
    - Now all archived entries will be displayed as a seperate chart

### 2.2.0

- Added Archive Functionality
    - Now all entries that are older than 3 Month will be summed up and archived for later use

### 2.1.0

- Added "Tendency"
    - "Tendency" is the difference between the average today and the average yesterday
- Added "Relative to yesterday"
    - "Relative to yesterday" is the difference between today and yesterday
- Added "Bounce Rate"
    - The "Bounce Rate" is calculated by the amount of users who visited the page only once divided by the amount of users who visited the page multiple times
- Added "Average Bounce Rate"
    - The "Average Bounce Rate" is the average Bounce Rate for the entire recorded history

### 2.0.0

Initial Version
