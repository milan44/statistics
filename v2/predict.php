<?php
header("Content-type: application/json");

define("LENGTH", 10);

require "vendor/autoload.php";

$sec = floatval(date('s'));
$sec += floatval(date('i')) * 60;
$sec += floatval(date('H')) * 60 * 60;

$mult = $sec / (60*60*24);

$log = json_decode($_POST["data"]);

if ($log === null)
{
    die('{
    "success": 0,
    "message": "No data"
}');
}

$predicted = [
    "success" => 1,
    "data" => [],
    "data2" => [],
    "data3" => [],
    "cached" => 0
];

// Bug fix
shell_exec('chmod +x ./vendor/php-ai/php-ml/bin/libsvm/*');

use Phpml\Regression\SVR;
use Phpml\SupportVectorMachine\Kernel;

function getReg()
{
    return new SVR(Kernel::LINEAR, $degree = 3, $epsilon=10.0);
}
function parseData($dat, $length = 1)
{
    $samples = [];
    $targets = [];

    for($x=$length;$x<sizeof($dat);$x++)
    {
        $sample = [];
        $targets[] = $dat[$x];
        for($y=1;$y<$length;$y++)
        {
            $sample[] = $dat[$x-$y];
        }
        $samples[] = array_reverse($sample);
    }

    return [
        'samples' => $samples,
        'targets' => $targets
    ];
}
function keepBetween(&$number, $min, $max)
{
	if ($number < $min)
	{
		$number = $min;
	}
	else if ($number > $max)
	{
		$number = $max;
	}
}
function getMax($array)
{
	$max = 0;
	foreach($array as $el)
	{
		if ($el > $max)
		{
			$max = $el;
		}
	}
	return $max;
}

$length = 10;
if (sizeof($log->data) < 15)
{
    $length = 1;
}

$dat = parseData($log->data, $length);

$samples = $dat['samples'];
$targets = $dat['targets'];

$regression = getReg();
$regression->train($samples, $targets);
$regression->train($samples, $targets);

$offset = 0;

$max = getMax($log->data) * 1.5;

$pred = intval($log->data[(sizeof($log->data)-1)] / $mult);
keepBetween($pred, 1, $max);
$targets[] = $pred;
$predicted["data"][] = round($pred);

for($x=1;$x<LENGTH;$x++)
{
    $last = [];
    for ($y=1;$y<=$length;$y++)
    {
        $last[] = $targets[(sizeof($targets)-$y)-$offset];
    }
    $pred = intval($regression->predict($last));

	keepBetween($pred, 1, $max);

    $targets[] = $pred;
    $predicted["data"][] = $pred;
}


$dat = parseData($log->data2, $length);

$samples = $dat['samples'];
$targets = $dat['targets'];

$regression = getReg();
$regression->train($samples, $targets);
$regression->train($samples, $targets);

$max = getMax($log->data2) * 1.5;

$pred = intval($log->data2[(sizeof($log->data2)-1)] / $mult);
keepBetween($pred, 1, $max);
$targets[] = $pred;
$predicted["data2"][] = $pred;

for($x=1;$x<LENGTH;$x++)
{
    $last = [];
    for ($y=1;$y<=$length;$y++)
    {
        $last[] = $targets[sizeof($targets)-$y];
    }
    $pred = intval($regression->predict($last));

	keepBetween($pred, 1, $max);
    $targets[] = $pred;
    $predicted["data2"][] = $pred;
}

$dat = parseData($log->data3, $length);

$samples = $dat['samples'];
$targets = $dat['targets'];

$regression = getReg();
$regression->train($samples, $targets);
$regression->train($samples, $targets);

$max = getMax($log->data3) * 1.5;

$pred = intval($log->data3[(sizeof($log->data3)-1)] / $mult);
keepBetween($pred, 1, $max);
$targets[] = $pred;
$predicted["data3"][] = round($pred);

for($x=1;$x<LENGTH;$x++)
{
    $last = [];
    for ($y=1;$y<=$length;$y++)
    {
        $last[] = $targets[sizeof($targets)-$y];
    }
    $pred = intval($regression->predict($last));

	keepBetween($pred, 1, $max);
    $targets[] = $pred;
    $predicted["data3"][] = $pred;
}

for ($x=0;$x<sizeof($predicted["data"]);$x++)
{
	if ($predicted["data3"][$x] > $predicted["data2"][$x])
	{
		$predicted["data2"][$x] = $predicted["data3"][$x];
	}
	if ($predicted["data2"][$x] > $predicted["data"][$x])
	{
		$predicted["data"][$x] = $predicted["data2"][$x];
	}
}

if (!isset($_POST["silent"]))
{
    echo json_encode($predicted);
}