(function() {
	var detection = {
		CGIProxy  : typeof window["_proxy_jslib_THIS_HOST"] !== 'undefined',
		Glype     : typeof ginf !== 'undefined',
		Cohula    : typeof window["REAL_PROXY_HOST"] !== 'undefined',
		HideMyAss : typeof window.dataLayer !== 'undefined' && window.dataLayer.filter(function(a) {return typeof a.proxyURL !== 'undefined'}).length !== 0,
		croxyproxy: typeof window["__cpn"] !== 'undefined'
	};
	
	window.usingProxy = Object.values(detection).includes(true);
})();
(function() {
	var SESSION_ID = Math.random().toString(36).substr(2, 9);
	
    var scriptPath = '/info/';
    $("script").each(function() {
        var path = this.src;
        if (path != null) {
            path = path.split('?')[0];
            if (path.endsWith('statistics.js')) {
                scriptPath = path.split('statistics.js')[0];
            }
        }
    });
    
	function getTimeZone() {
		if (Intl === undefined || Intl === null) {
			return "undefined";
		}
		if (Intl.DateTimeFormat === undefined || Intl.DateTimeFormat === null) {
			return "undefined";
		}
		if (Intl.DateTimeFormat() === undefined || Intl.DateTimeFormat() === null) {
			return "undefined";
		}
		if (Intl.DateTimeFormat().resolvedOptions === undefined || Intl.DateTimeFormat().resolvedOptions === null) {
			return "undefined";
		}
		if (Intl.DateTimeFormat().resolvedOptions() === undefined || Intl.DateTimeFormat().resolvedOptions() === null) {
			return "undefined";
		}
		if (Intl.DateTimeFormat().resolvedOptions().timeZone === undefined || Intl.DateTimeFormat().resolvedOptions().timeZone === null) {
			return "undefined";
		}

		return Intl.DateTimeFormat().resolvedOptions().timeZone;
	}
    
    function getPlatformType() {
        if(navigator.userAgent.match(/mobile/i)) {
            return 'Mobile';
        } else if (navigator.userAgent.match(/iPad|Android|Touch/i)) {
            return 'Tablet';
        } else {
            return 'Desktop';
        }
    }
	
	var additional = '';
	
	if (typeof window.usingProxy !== 'undefined' && window.usingProxy) {
		additional = '&proxy=1';
	}

    var n = new XMLHttpRequest;
    n.open("GET", scriptPath + "include.php?path="+encodeURIComponent(window.location.pathname)+"&zone="+encodeURIComponent(getTimeZone())+"&agent="+encodeURIComponent(getPlatformType())+additional);
    n.send();

    function updateRealTime() {
        var x = new XMLHttpRequest;
        x.open("GET", scriptPath + "realtime.php?path="+encodeURIComponent(window.location.pathname)+"&zone="+encodeURIComponent(getTimeZone())+"&id="+encodeURIComponent(SESSION_ID));
        x.send();
    }

    updateRealTime();
    setInterval(updateRealTime, 10000);
})();