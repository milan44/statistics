<?php

PluginHelper::addLessFile("style/", "style.less");

$rows = [
    new TableRow("today", "Today"),
    new TableRow("avgtoday", "Average Today"),
    new TableRow("maximum", "Maximum"),
    new TableRow("avgtendency", "Tendendcy"),
    new TableRow("bounce", "Bounce Rate"),
    new TableRow("loyalty", "User Loyalty"),
    new TableRow("persec", "Per second"),
    new TableRow("avgpersec", "Average per second"),
    new TableRow("avgtime", "Average Time a user spends on your site every day (hh:mm:ss)"),
];

foreach($rows as $row) {
    PluginHelper::addInformationRow($row);
}
