<?php

$delete = [
    "../cache/index.html",
    "../cache/login.css",
    "../cache/style.css"
];

foreach($delete as $file)
{
    if (file_exists($file))
        unlink($file);
}