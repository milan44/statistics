<?php

require "../lib/Runner.php";
require "../lib/ConnectionBuilder.php";
require "../lib/LogEntry.php";
require "../lib/Toolbox.php";
include "../config.php";

require "NoiseWriter.php";
require "PerlinNoise.php";

function getIP()
{
    return md5("".rand());
}

$connt = null;
    
ConnectionBuilder::execute($connt, $username, $password, $database);

function getRandomPath()
{
	$a = [
		"/",
		"/shop/",
		"/help/",
		"/products/",
		"/about-us/"
	];
	
	return base64_encode($a[array_rand($a)]);
}
function getRandomZone()
{
	$a = [
		"Europe/Moscow",
		"America/New_York",
		"America/Los_Angeles",
		"Europe/Berlin",
		"UTC",
		"America/Chicago"
	];
	
	return base64_encode($a[array_rand($a)]);
}
function getRandomAgent()
{
	$a = [
		"Tablet",
		"Mobile",
		"Desktop"
	];
	
	return base64_encode($a[array_rand($a)]);
}

$log = [];

$amount = 100;

if (isset($_GET["amount"]))
    $amount = intval($_GET["amount"]);

$noise = new NoiseGenerator\PerlinNoise();

$day = rand(1500, 2000);
$startDate = new DateTime();
$startDate->setTimestamp(time());

$index = 1;
for ($x=0;$x<$amount;$x++)
{
    $startDate->setTime(mt_rand(0, 23), 0);
    $date = $startDate->getTimestamp();
    $ip = getIP();
    
    $log[] = new LogEntry($ip, $date, LogType::$HIT, getRandomPath(), getRandomZone(), getRandomAgent());
    if ($x % rand(1, 2) == 0)
    {
        $log[] = new LogEntry($ip, $date, LogType::$SESSION);
    }
    if ($x % rand(2, 3) == 0)
    {
        $log[] = new LogEntry($ip, $date, LogType::$NEWUSER);
    }
    
    if ($x % $day === 0)
    {
		$index++;
        $day += rand(2500, 3000);
        $startDate->sub(new DateInterval('P1D'));
    }
}

Runner::persist($log);

$connt->close();
?>