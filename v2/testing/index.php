<html>
    <head>
        <title>
            Test Controller
        </title>
        <script src="../js/jquery-1.11.1.min.js"></script>
        <style>
            a {
                color: blue;
                text-decoration: none;
            }
        </style>
    </head>
    <body>
        <a id="empty" href="#">Empty out Entries</a>
        <br>
        <br>
        <a id="cache" href="#">Delete Cache</a>
        <br>
        <br>
        Amount of Data: <input type="number" id="amount" value="0" min="0" max="50000" /> <a id="testdata" href="#">Generate Testdata</a>
        
        <br><br><br>
        
        <pre><code id="log"></code><br><a id="clearlog" href="#">Clear Log</a></pre>
        
        <script>
            var request = null;
            
            function log(text) {
                var date = new Date();
                var dd = date.getDate();
                var yyyy = date.getFullYear();
                var mm = date.getMonth() + 1;
                var hh = date.getHours();
                var m = date.getMinutes();
                var ss = date.getSeconds();
                
                if (dd < 10) {
                    dd = '0' + dd;
                } 
                if (mm < 10) {
                    mm = '0' + mm;
                }
                if (hh < 10) {
                    hh = '0' + hh;
                } 
                if (m < 10) {
                    m = '0' + m;
                } 
                if (ss < 10) {
                    ss = '0' + ss;
                }
                
                var format = "[" + dd + "." + mm + "." + yyyy + " - " + hh + ":" + m + ":" + ss + "]";
                
                text = format + " " + text;
                
                $("#log").html(text + "\n" + $("#log").html());
            }
            
            $("#clearlog").on("click", function(e) {
                e.preventDefault();
                $("#log").html("");
            });
            $("#empty").on("click", function(e) {
                e.preventDefault();
                if (request != null)
                    return;
                
                var before = "Empty out Entries";
                
                $(this).html("Loading......");
                
                request = $.ajax({
                    method: "GET",
                    url: "empty.php",
                    complete: function() {
                        $("#empty").html(before);
                        log("Emptied out Entries");
                        request = null;
                    }
				});
            });
            $("#testdata").on("click", function(e) {
                e.preventDefault();
                if (request != null)
                    return;
                
                var before = "Generate Testdata";
                var amount = parseInt($("#amount").val());
                
                $(this).html("Loading......");
                
                request = $.ajax({
                    method: "GET",
                    url: "testdata.php?amount="+amount,
                    complete: function() {
                        $("#testdata").html(before);
                        log("Generated " + amount + " Testdata");
                        request = null;
                    }
				});
            });
            $("#cache").on("click", function(e) {
                e.preventDefault();
                if (request != null)
                    return;
                
                var before = "Delete Cache";
                
                $(this).html("Loading......");
                
                request = $.ajax({
                    method: "GET",
                    url: "delete.php",
                    complete: function() {
                        $("#cache").html(before);
                        log("Deleted Cache");
                        request = null;
                    }
				});
            });
        </script>
    </body>
</html>