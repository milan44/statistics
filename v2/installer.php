<?php
session_start();
if (file_exists(".installed"))
{    
    header("Location: login.php");
    exit;
}

require_once "lib/ext/lessc.inc.php";
require "autoload.php";

$css = Grabber::compile("style/style.less");
$login = Grabber::compile("style/login.less");

if (!file_exists('cache')) {
    mkdir('cache', 0777, true);
}

file_put_contents("cache/style.css", $css);
file_put_contents("cache/login.css", $login);

if (isset($_POST["db"]) && isset($_POST["user"]) && isset($_POST["pass"]))
{
    header("Content-type: text/plain");
    $connt = null;
    
    ConnectionBuilder::execute($connt, $_POST["user"], $_POST["pass"], $_POST["db"]);
    
    $connt->close();
    
    
    echo '{"s": "true"}';
    exit;
}

if (isset($_POST["settings"]))
{
    $settings = $_POST["settings"];
    
    if (empty(trim($settings["ht_loc"])))
    {
        $settings["ht_loc"] = "./unused.htaccess";
    }
    
    $config = "<?php\n\$username = '".$settings["username"]."';\n\$password = '".$settings["password"]."';\n\$database = '".$settings["db"]."';\n\$htacces = [\n    'location' => '".$settings["ht_loc"]."',\n    'tor' => '".$settings["ht_tor"]."',\n    'proxy' => '".$settings["ht_prox"]."'\n];\n?>";
    $admin = $settings["username_admin"].":".hash('sha512', $settings["password_admin"]);
    
    file_put_contents("config.php", $config);
    file_put_contents("users.txt", $admin);
    file_put_contents(".installed", "");
    
    header("Location: login.php");
    exit;
}

?>
<html>
    <head>
    	<title>Statistics System - Installer</title>
    	<link rel="stylesheet" href="cache/login.css" />
        <script src="js/jquery-1.11.1.min.js"></script>
    </head>
    <body>
    	<div class="form">
    
    		<div class="tab-content">
    			<div id="login">
    				<h1>Installer</h1>
    
    				<form action="installer.php" method="post" id="form">
    
    					<div class="field-wrap" id="errormsg" style="color: red;">
                            
    					</div>
                        
                        <h2>Database Settings</h2>
                        
                        <div class="field-wrap" id="db_errormsg" style="color: red; font-weight: bold;">
                            
    					</div>
    
    					<div class="field-wrap">
    						<input name="settings[db]" type="text" autocomplete="off" placeholder="Database" required />
    					</div>
    
    					<div class="field-wrap">
    						<input name="settings[username]" type="text" autocomplete="off" placeholder="Username" required />
    					</div>
    
    					<div class="field-wrap">
    						<input name="settings[password]" type="password" autocomplete="off" placeholder="Password" required />
    					</div>
    					
    					<div class="field-wrap">
    						<input name="settings[ht_loc]" type="text" autocomplete="off" placeholder="Location of your .htaccess file" required />
    					</div>
    
    					<div class="field-wrap">
    						<input name="settings[ht_tor]" type="url" autocomplete="off" placeholder="Where you want tor users to be redirected to." required />
    					</div>
    
    					<div class="field-wrap">
    						<input name="settings[ht_prox]" type="url" autocomplete="off" placeholder="Where you want proxy users to be redirected to." required />
    					</div>
    
    					<button type="button" class="button button-block" style="font-size: 15px;" id="connection">Check Connection</button>
                        
                        <h2>Admin Settings</h2>
    
    					<div class="field-wrap">
    						<input name="settings[username_admin]" type="text" autocomplete="off" placeholder="Username" value="admin" required />
    					</div>
    
    					<div class="field-wrap">
    						<input name="settings[password_admin]" type="password" autocomplete="off" placeholder="Password" value="password" required />
    					</div>
                        
    					<button type="button" class="button button-block" id="install">Install</button>
    
    				</form>
                    <script>
                        $("#install").on("click", function() {
                            var database = $("input[name='settings[db]']").val();
                            var username = $("input[name='settings[username]']").val();
                            var password = $("input[name='settings[password]']").val();
                            
                            $("#form").find("input, button").prop("disabled", true);
                            
                            $("#db_errormsg").css("color", "yellow");
                            $("#db_errormsg").html("Testing connection.....");
                            
                            var data = {
                                "db": database,
                                "user": username,
                                "pass": password
                            };
                            
                            var request = $.post("installer.php", data);
                            
                            request.done(function (response, textStatus, jqXHR){
                                if (!response.startsWith("{"))
                                    response = '{"s": "false"}';
                                
                                response = JSON.parse(response);
                                
                                if (response.s === "true") {
                                    $("#db_errormsg").css("color", "green");
                                    $("#db_errormsg").html("Connection successful!");
                                    $("#install").html("Installing.....");
                                    
                                    setTimeout(function() {
                                        var req = $.post('installer.php', $('#form').serialize());
                                        req.done(function (response, textStatus, jqXHR){
                                            window.location.href = "login.php";
                                        });

                                        req.fail(function (jqXHR, textStatus, errorThrown){
                                            $("#errormsg").html("An error accured while installing!");
                                            console.error(errorThrown);
                                        });
                                        
                                        req.always(function() {
                                            $("#install").html("Install");
                                        });
                                    }, 250);
                                    
                                } else {
                                    $("#db_errormsg").css("color", "red");
                                    $("#db_errormsg").html("Connection failed!");
                                }
                            });

                            request.fail(function (jqXHR, textStatus, errorThrown){
                                $("#db_errormsg").html("An error accured!");
                                console.error(errorThrown);
                            });

                            request.always(function () {
                                $("#form").find("input, button").prop("disabled", false);
                            });
                        });
                        $("#connection").on("click", function() {
                            var database = $("input[name='settings[db]']").val();
                            var username = $("input[name='settings[username]']").val();
                            var password = $("input[name='settings[password]']").val();
                            
                            $("#form").find("input, button").prop("disabled", true);
                            
                            $("#db_errormsg").css("color", "yellow");
                            $("#db_errormsg").html("Testing connection.....");
                            
                            var data = {
                                "db": database,
                                "user": username,
                                "pass": password
                            };
                            
                            var request = $.post("installer.php", data);
                            
                            request.done(function (response, textStatus, jqXHR){
                                if (!response.startsWith("{"))
                                    response = '{"s": "false"}';
                                
                                response = JSON.parse(response);
                                
                                if (response.s === "true") {
                                    $("#db_errormsg").css("color", "green");
                                    $("#db_errormsg").html("Connection successful!");
                                } else {
                                    $("#db_errormsg").css("color", "red");
                                    $("#db_errormsg").html("Connection failed!");
                                }
                            });

                            request.fail(function (jqXHR, textStatus, errorThrown){
                                $("#db_errormsg").html("An error accured!");
                                console.error(errorThrown);
                            });

                            request.always(function () {
                                $("#form").find("input, button").prop("disabled", false);
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </body>
</html>