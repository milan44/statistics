var ISCACHED = true;

$(document).ready(function() {
	var errorMessage = 'Statistics error, please contact your system administrator!<br><a href="#" id="show_error">Show Error</a>';
	
	function IsJsonString(str) {
	    try {
	        JSON.parse(str);
	    } catch (e) {
	        return false;
	    }
	    return true;
	}
	function checkIfSet(field) {
		var url = window.location.href;
		if(url.indexOf('?' + field) != -1)
		    return true;
		else if(url.indexOf('&' + field) != -1)
		    return true;
		return false;
	}
	
	var cached = true;
	var result = '';
	
	if (checkIfSet('g')) {
		$('.initial p').html('Refreshing Cache...');
		result = $.ajax({
	        type: "GET",
	        url: 'index.php?new',
	        async: false
	    }).responseText;
		
		var time = parseFloat($('#cacheTime').html());
		time += JSON.parse(result).time;
		$('#cacheTime').html((time+'').split('.')[0]+'.'+(time+'').split('.')[1].substring(0, 6));
		
		cached = false;
        ISCACHED = false;
	}
	
	window.history.replaceState(null, null, window.location.pathname);
	
	function getJson() {
	    return $.ajax({
	        type: "GET",
	        url: 'cache/data.json',
	        async: false
	    }).responseText;
	}
	$('.initial p').html('Loading Data...');
	
	var json = getJson();
	
	function setErrorLevel(level) {
		$('.initial').removeClass('warning');
		$('.initial').removeClass('error');
		$('.initial').addClass(level);
		
		if (level === 'error') {
			$('.initial').append(`<div class="head">
	<div class="face face__sad">
        <div class="eye-left"></div>
        <div class="eye-right"></div>
        <div class="mouth"></div>
    </div>
</div>`);
			$('.initial .sk-cube').remove();
			
			$('.initialWrapper').append(`<div class="error_msg"><span id="close_error" class="close rounded thick"></span>
	`+result+`
</div>`);
			$("#close_error").on("click", function() {
				$(this).parent().css("display", "none");
			});
			$("#show_error").on("click", function(e) {
				e.preventDefault();
				$('.error_msg').css("display", "block");
			});
		}
	}
	
	if (!IsJsonString(json)) {
		if (!cached) {
			$('.initial p').html(errorMessage);
			setErrorLevel('error');
			return;
		}
		
		setErrorLevel('warning');
		$('.initial p').html('Data Loading error, refreshing Cache...');
		
		result = $.ajax({
	        type: "GET",
	        url: 'index.php?new',
	        async: false
	    }).responseText;
		
		var time = parseFloat($('#cacheTime').html());
		time += JSON.parse(result).time;
		$('#cacheTime').html((time+'').split('.')[0]+'.'+(time+'').split('.')[1].substring(0, 6));
		
		if (!IsJsonString(result)) {
			$('.initial p').html(errorMessage);
			setErrorLevel('error');
			return;
		}

		setErrorLevel('warning');
		$('.initial p').html('Trying to load Data...');
		
		json = getJson();
		
		if (!IsJsonString(json)) {
			$('.initial p').html(errorMessage);
			setErrorLevel('error');
			return;
		}
	}
	
    var backup = $.ajax({
        type: "GET",
        url: 'index.php?backup',
        async: false
    }).responseText;
    
	json = JSON.parse(json);
	
	$("head").append(json.customcss);
	
	$("additionalrows").replaceWith(`<table class="table">
	<thead>
		<tr>
			<th></th>
			<th><h1>New Users</h1></th>
			<th><h1>Sessions</h1></th>
			<th><h1>Hits</h1></th>
		</tr>
	</thead>
	<tbody>
		`+json.additionalrows+`
	</tbody>
</table>`);
    
    $("backup").replaceWith(backup);
	
	$.each(json.vars, function(key, value) {
		window[key] = value;
	});
	
	$("name").replaceWith(json.name);
	$("timezone").replaceWith(json.timezone);
	
	$("customjs").replaceWith('<script src="'+json.customjs+'"></script>');

	setTimeout(function() {
		$(".initialWrapper").addClass("disappear");
		setTimeout(function() {
		    $(".initialWrapper").remove();
		}, 600);
	}, 200);
});