function clone(obj) {
    var getCircularReplacer = () => {
        var seen = new WeakSet;
        return (key, value) => {
            if (typeof value === "object" && value !== null) {
                if (seen.has(value)) {
                    return;
                }
                seen.add(value);
            }
            return value;
        };
    };
    return JSON.parse(JSON.stringify(obj, getCircularReplacer()));
}

function getChartOptions() {
	return {
		devicePixelRatio: 2,
    	tooltips: {
        	mode: 'index',
        	intersect: false
    	},
    	hover: {
			mode: 'nearest',
			intersect: true
		},
    	maintainAspectRatio: false,
    	scales: {
	        yAxes: [{
	            display: true,
	            ticks: {
	                beginAtZero: true,
	                max: Math.ceil(max/10)*10,
	            },
	            gridLines: {
				  	display: true,
				  	color: "#364363"
				}
	        }],
	        xAxes: [{
	            gridLines: {
				  	display: true,
				  	color: "#364363"
				}
	        }]
	    }
    };
}

$("#cachebutton").on("click", function() {
    $(this).html("Loading...");
    window.location.replace("index.php?g");
});