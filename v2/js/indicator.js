(function($) {
    var line = null;

    $(".chart:not(.noLine)").on("mouseenter", function(e) {
        $(this).addClass("active");
        var left = e.pageX - $(this).offset().left;
        var height = $(this).height();
        line = document.createElement("div");
        line.style.height = height + "px";
        line.style.position = "absolute";
        line.style.left = left + "px";
        line.style.top = "0px";
        line.style.backgroundColor = "black";
        line.style.width = 1 + "px";
        line.style.pointerEvents = "none";
        
        $(this).parent().append(line);

        $(this).on("mouseleave", function() {
            if (line != null) {
                $(line).remove();
                $(this).removeClass("active");
            }
        });

        $(this).on("mousemove", function(e) {
            if (line != null) {
                var left = e.pageX - $(this).offset().left;
                line.style.left = left + "px";
            }
        });
    });
})($);