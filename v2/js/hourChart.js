(function($) {
	var options = getChartOptions();
	var canvas = document.getElementById("hourChart");
	
	var maxVal = 0;
	
	for (x=0;x<hours.datasets[0].data.length;x++) {
		var hour = hours.datasets[0].data[x];
		if (hour > maxVal) {
			maxVal = hour;
		}
	}
	
	options.scales.yAxes[0].ticks.max = maxVal;
	options.scales.yAxes[0].display = false;
	options.scales.yAxes[0].gridLines.display = false;
	options.scales.xAxes[0].gridLines.display = false;
	options.tooltips.mode = "nearest";
	options.tooltips.intersect = true;
	options.scales.xAxes[0].barPercentage = 1;
	options.scales.xAxes[0].categoryPercentage = 0.95;
	
	var ctx = canvas.getContext('2d');
	var chart = new Chart(ctx, {
	    type: 'bar',
	    data: hours,
	    options: options
	});
})($);