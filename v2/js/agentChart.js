(function($) {
	var options = getChartOptions();
	var canvas = document.getElementById("agentChart");
	var canvasToday = document.getElementById("agentChartToday");

    options.devicePixelRatio = 2;
    
    var agents = {
        labels: [],
        datasets: [
            {
                label: "Device Type (All Time)",
                data: [],
                borderWidth: 0,
                backgroundColor: "rgba(24, 88, 117, 0.8)"
            }
        ]
    };
    
    var agentsToday = clone(agents);
    agentsToday.datasets[0].label = "Device Type (Yesterday)";
    
    for (var x=0;x<topagents.all.length;x++) {
        var agent = topagents.all[x];
        agents.labels.push(agent.agent);
        agents.datasets[0].data.push(agent.count);
    }
    for (var x=0;x<topagents.today.length;x++) {
        var agent = topagents.today[x];
        agentsToday.labels.push(agent.agent);
        agentsToday.datasets[0].data.push(agent.count);
    }
	
	options.scales.yAxes[0].display = false;
	options.scales.yAxes[0].gridLines.display = false;
	options.scales.xAxes[0].gridLines.display = false;
	options.tooltips.mode = "nearest";
	options.tooltips.intersect = true;
	options.scales.xAxes[0].barPercentage = 1;
	options.scales.xAxes[0].categoryPercentage = 0.95;
	
    options.scales.yAxes[0].ticks.max = getMax(agents.datasets[0].data);
    
	var ctx = canvas.getContext('2d');
	var chart = new Chart(ctx, {
	    type: 'bar',
	    data: agents,
	    options: clone(options)
	});
	
    options.scales.yAxes[0].ticks.max = getMax(agentsToday.datasets[0].data);
    
    ctx = canvasToday.getContext('2d');
	chart = new Chart(ctx, {
	    type: 'bar',
	    data: agentsToday,
	    options: clone(options)
	});
    
    function getMax(data) {
        var maxVal = 0;
	
        for (x=0;x<data.length;x++) {
            var dat = data[x];
            if (dat > maxVal) {
                maxVal = dat;
            }
        }
        
        return maxVal;
    }
})($);