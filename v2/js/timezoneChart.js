(function($) {
	var options = getChartOptions();
	var canvas = document.getElementById("zoneChart");
	var canvasToday = document.getElementById("zoneChartToday");
    
    function compare(a, b){
        if (a.count > b.count) return -1;
        if (b.count > a.count) return 1;

        return 0;
    }
    
    topzones.all = topzones.all.sort(compare).slice(0, 6);
    topzones.today = topzones.today.sort(compare).slice(0, 6);

    options.devicePixelRatio = 2;
    
    var zones = {
        labels: [],
        datasets: [
            {
                label: "Top 6 Timezones (All Time)",
                data: [],
                borderWidth: 0,
                backgroundColor: "rgba(24, 88, 117, 0.8)"
            }
        ]
    };
    
    var zonesToday = clone(zones);
    zonesToday.datasets[0].label = "Top 6 Timezones (Yesterday)";
    
    for (var x=0;x<topzones.all.length;x++) {
        var zone = topzones.all[x];
        zones.labels.push(zone.zone);
        zones.datasets[0].data.push(zone.count);
    }
    for (var x=0;x<topzones.today.length;x++) {
        var zone = topzones.today[x];
        zonesToday.labels.push(zone.zone);
        zonesToday.datasets[0].data.push(zone.count);
    }
	
	options.scales.yAxes[0].display = false;
	options.scales.yAxes[0].gridLines.display = false;
	options.scales.xAxes[0].gridLines.display = false;
	options.tooltips.mode = "nearest";
	options.tooltips.intersect = true;
	options.scales.xAxes[0].barPercentage = 1;
	options.scales.xAxes[0].categoryPercentage = 0.95;
	
    options.scales.yAxes[0].ticks.max = getMax(zones.datasets[0].data);
    
	var ctx = canvas.getContext('2d');
	var chart = new Chart(ctx, {
	    type: 'bar',
	    data: zones,
	    options: clone(options)
	});
	
    options.scales.yAxes[0].ticks.max = getMax(zonesToday.datasets[0].data);
    
    ctx = canvasToday.getContext('2d');
	chart = new Chart(ctx, {
	    type: 'bar',
	    data: zonesToday,
	    options: clone(options)
	});
    
    function getMax(data) {
        var maxVal = 0;
	
        for (x=0;x<data.length;x++) {
            var dat = data[x];
            if (dat > maxVal) {
                maxVal = dat;
            }
        }
        
        return maxVal;
    }
})($);