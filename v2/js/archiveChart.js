(function($) {
    if (archivedEntries === null) {
        document.getElementById("archiveChart").parentElement.style.display = "none";
        return;
    }
    
    var opts = getChartOptions();
    
    var max = 0;
    
    for (var x=0;x<archivedEntries.labels.length;x++) {
        var a = archivedEntries.datasets[0].data[x];
        var b = archivedEntries.datasets[1].data[x];
        var c = archivedEntries.datasets[2].data[x];
        
        if (max < a) {
            max = a;
        }
        if (max < b) {
            max = b;
        }
        if (max < c) {
            max = c;
        }
    }
    
    opts.scales.yAxes[0].ticks.max = max;
    
    var ctx = document.getElementById("archiveChart").getContext('2d');
    var chart = new Chart(ctx, {
        type: 'line',
        data: archivedEntries,
        options: opts
    });
})($);