(function($) {
    function color(percentage) {
        if (percentage < 0) {
            return '<span style="color: red">'+percentage;
        } else {
        	return '<span style="color: green">'+percentage;
        }
    }

    function bounceText(percentage) {
        if (percentage <= 26) {
            return "Unlikely";
        } else if (percentage <= 40) {
            return "Excellent";
        } else if (percentage <= 55) {
            return "Average";
        } else if (percentage <= 70) {
            return "Higher than Average";
        }

        return "Disappointing";
    }

	var hitsToday = data.datasets[2].data[data.datasets[2].data.length-9];
	var sessToday = data.datasets[1].data[data.datasets[1].data.length-9];
	var newsToday = data.datasets[0].data[data.datasets[0].data.length-9];

	var avghitsToday = avgdata.datasets[2].data[avgdata.datasets[2].data.length-1];
	var avgsessToday = avgdata.datasets[1].data[avgdata.datasets[1].data.length-1];
	var avgnewsToday = avgdata.datasets[0].data[avgdata.datasets[0].data.length-1];

    var avghitsYesterday = avgdata.datasets[2].data[avgdata.datasets[2].data.length-2];
	var avgsessYesterday = avgdata.datasets[1].data[avgdata.datasets[1].data.length-2];
	var avgnewsYesterday = avgdata.datasets[0].data[avgdata.datasets[0].data.length-2];

	var avghitsPreYesterday = avgdata.datasets[2].data[avgdata.datasets[2].data.length-3];
	var avgsessPreYesterday = avgdata.datasets[1].data[avgdata.datasets[1].data.length-3];
	var avgnewsPreYesterday = avgdata.datasets[0].data[avgdata.datasets[0].data.length-3];

    var hitsPreYesterday = data.datasets[2].data[data.datasets[2].data.length-11];
	var sessPreYesterday = data.datasets[1].data[data.datasets[1].data.length-11];
	var newsPreYesterday = data.datasets[0].data[data.datasets[0].data.length-11];

	var hitsYesterday = data.datasets[2].data[data.datasets[2].data.length-10];
	var sessYesterday = data.datasets[1].data[data.datasets[1].data.length-10];
	var newsYesterday = data.datasets[0].data[data.datasets[0].data.length-10];

    var hitsTendency = Math.round(((avghitsYesterday/avghitsPreYesterday) - 1)*10000)/100;
    var sessTendency = Math.round(((avgsessYesterday/avgsessPreYesterday) - 1)*10000)/100;
    var newsTendency = Math.round(((avgnewsYesterday/avgnewsPreYesterday) - 1)*10000)/100;

	$("#today td:nth-child(4)").html(hitsToday);
	$("#today td:nth-child(3)").html(sessToday);
	$("#today td:nth-child(2)").html(newsToday);

	$("#maximum td:nth-child(4)").html(maxhit);
	$("#maximum td:nth-child(3)").html(maxses);
	$("#maximum td:nth-child(2)").html(maxnew);

	$("#avgtoday td:nth-child(4)").html(avghitsToday);
	$("#avgtoday td:nth-child(3)").html(avgsessToday);
	$("#avgtoday td:nth-child(2)").html(avgnewsToday);

	$("#avgtendency td:nth-child(4)").html(color(hitsTendency) + "%</span>");
	$("#avgtendency td:nth-child(3)").html(color(sessTendency) + "%</span>");
	$("#avgtendency td:nth-child(2)").html(color(newsTendency) + "%</span>");


	$("#bounce td:nth-child(2)").html(bounce.today + "% (" + bounceText(bounce.today) + ")</span>");

	$("#loyalty td:nth-child(2)").html(loyalty + "%</span>");

	$("#avgtime td:nth-child(2)").html(averageTime);


    $("#persec td:nth-child(4)").html(persecond.hits);
	$("#persec td:nth-child(3)").html(persecond.sessions);
	$("#persec td:nth-child(2)").html(persecond.users);

    $("#avgpersec td:nth-child(4)").html(avgpersecond.hits);
	$("#avgpersec td:nth-child(3)").html(avgpersecond.sessions);
	$("#avgpersec td:nth-child(2)").html(avgpersecond.users);
})($);
