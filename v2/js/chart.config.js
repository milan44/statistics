/*
 * Global Chart Configuration
 */

Chart.defaults.global.animation.easing = 'easeOutQuint';
Chart.defaults.global.animation.duration = 2000;

Chart.defaults.global.tooltips.backgroundColor = 'rgba(108, 129, 147, 0.9)';

Chart.defaults.global.tooltips.borderColor = 'rgb(65, 77, 88)';
Chart.defaults.global.tooltips.borderWidth = 1;

Chart.defaults.global.elements.rectangle.borderWidth = 2;

Chart.defaults.global.defaultColor = 'rgba(167, 161, 174, 1)';

Chart.defaults.global.elements.point.radius = 1;

Chart.defaults.global.defaultFontFamily = "Ubuntu";