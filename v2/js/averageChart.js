(function($) {
    var ctx = document.getElementById("avgChart").getContext('2d');
    var chart = new Chart(ctx, {
        type: 'line',
        data: avgdata,
        options: getChartOptions()
    });
})($);