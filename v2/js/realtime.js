(function() {
    var timeout = 0;
    var waitFor = 10;

    function getStats(data, title) {
        var sorted = [];
        for (var dat in data) {
            sorted.push({
                key: dat,
                value: data[dat]
            });
        }
        sorted.sort(function(a, b) {
            return b.value - a.value;
        });

        var html = "<h4>"+title+"</h4>";
        for (var x = 0; x < sorted.length; x++) {
            if (x === 3) {
                break;
            }
            var s = sorted[x];

            html += '<br>'+(x+1)+'. ';
            if (s.key.startsWith("/")) {
                html += '<a style="color: gray" href="'+s.key+'" target="_blank">'+s.key+'</a>';
            } else {
                html += s.key;
            }
            html += ' ('+s.value+' User)';
        }

        return html;
    }

    function errorReal(error) {
        $("#realtime").html('<b style="color: red">'+error+'</b>');
    }
    function displayResult(data) {
        $("#realtime").html(data.amount + " User online");
        $("#realpages").html(getStats(data.pages, "Realtime Pages"));
        $("#realzones").html(getStats(data.zones, "Realtime Timezones"));
    }

    function updateRealTime() {
        if (timeout === 0) {
            $("#realNext").html(', Updating...');
            $.ajax({
                cache: false,
                dataType: "json",
                url: "realtime.php?get",
                success: function(data) {
                    if (data.success) {
                        displayResult(data);
                    } else {
                        errorReal(data.error);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    errorReal(textStatus);
                }
            });
            timeout = waitFor;
        }
        $("#realNext").html(', Next Update in ' + timeout);

        timeout--;
        setTimeout(updateRealTime, 1000);
    }

    function updateNow() {
        timeout = waitFor;
        $("#realNext").html(', Updating...');
        $.ajax({
            cache: false,
            dataType: "json",
            url: "realtime.php?get",
            success: function(data) {
                if (data.success) {
                    displayResult(data);
                } else {
                    errorReal(data.error);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                errorReal(textStatus);
            }
        });
    }

    $("#realnow").on("click", function(e) {
        e.preventDefault();
        updateNow();
    });

    updateRealTime();
})();
