(function() {
    function compare(a, b){
        if (a.count > b.count) return -1;
        if (b.count > a.count) return 1;

        return 0;
    }
    
    function getLink(piece) {
        return '<a style="color: gray" href="'+piece+'" target="_blank">'+piece+'</a>';
    }
    function getLeaderBoard(array, withlink) {
    	if (withlink === undefined) {
	        var html = "1. " + getLink(array[0].path) + " ("+array[0].count+" Hits)";
	        
	        if (array.length > 1) {
	            html += "<br>2. " + getLink(array[1].path) + " ("+array[1].count+" Hits)";
	        }
	        if (array.length > 2) {
	            html += "<br>3. " + getLink(array[2].path) + " ("+array[2].count+" Hits)";
	        }
	        
	        return html;
    	}
    	var html = "1. " + array[0].zone + " ("+array[0].count+" Hits)";
        
        if (array.length > 1) {
            html += "<br>2. " + array[1].zone + " ("+array[1].count+" Hits)";
        }
        if (array.length > 2) {
            html += "<br>3. " + array[2].zone + " ("+array[2].count+" Hits)";
        }
        
        return html;
    }
    
    toppaths.all.sort(compare);
    toppaths.today.sort(compare);
    
    if (toppaths.all.length !== 0) {
        var html = getLeaderBoard(toppaths.all);
        $("#topall").html(html);
    } else {
        $("#topall").html("No Data");
    }
    
    if (toppaths.today.length !== 0) {
        var html = getLeaderBoard(toppaths.today);
        $("#topyesterday").html(html);
    } else {
        $("#topyesterday").html("No Data");
    }
    
    
    /*topzones.all.sort(compare);
    topzones.today.sort(compare);
    
    if (topzones.all.length !== 0) {
        var html = getLeaderBoard(topzones.all, false);
        $("#zoneall").html(html);
    } else {
        $("#zoneall").html("No Data");
    }
    
    if (topzones.today.length !== 0) {
        var html = getLeaderBoard(topzones.today, false);
        $("#zoneyesterday").html(html);
    } else {
        $("#zoneyesterday").html("No Data");
    }*/
})();