var mainChart = null;
var mainChartRendered = false;

(function($) {
    var ctx = document.getElementById("mainChart").getContext('2d');
    var options = getChartOptions();
    
    options.animation = {
        onComplete: function() {
            mainChartRendered = true
        },
        duration: 0
    };
	
	var vMax = 0;
	for (x=0;x<data.datasets.length;x++) {
		for (y=0;y<data.datasets[x].data.length;y++) {
			if (data.datasets[x].data[y] > vMax) {
				vMax = data.datasets[x].data[y];
			}
		}
	}
	
	options.scales.yAxes[0].ticks.max = vMax;
    
    mainChart = new Chart(ctx, {
        type: 'line',
        data: clone(data),
        options: options
    });
})($);