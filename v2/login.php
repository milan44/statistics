<?php
session_start();

if (!file_exists(".installed"))
{
    header("Location: installer.php");
    exit;
}

include "lib/Error.php";
include "lib/Login.php";

if ((isset($_SESSION["login"]) && $_SESSION["login"] == true) || checkCookie())
{
    $connt->close();
    header("Location: index.php?".$_SERVER['QUERY_STRING']);
    exit;
}

if (isset($_POST["username"]) && isset($_POST["password"]))
{
    $username = $_POST["username"];
    $password = hash('sha512', $_POST["password"]);

    $logins = file_get_contents("users.txt");
    $logins = preg_split('/$\R?^/m', $logins);

    $l = false;

    foreach ($logins as $login)
    {
        $split = explode(":", $login);
        if ($split[0] === $username && $split[1] === $password)
        {
            $l = true;
            break;
        }
    }
    header("Content-type: application/json");
    if ($l)
    {
        $_SESSION["login"] = true;
        $_SESSION["name"] = $username;

        setLoginCookie($username);

        echo '{"s": "true"}';
    }
    else
    {
        echo '{"s": "false"}';
    }
    $connt->close();
    exit;
}
$connt->close();
?>
<html>
    <head>
    	<title>Statistics System - Login</title>
		<link rel="shortcut icon" type="image/png" href="./favicon.png">
    	<link rel="stylesheet" href="cache/login.css" />
        <script src="js/jquery-1.11.1.min.js"></script>
    </head>
    <body>
    	<div class="form">

    		<div class="tab-content">
    			<div id="login">
    				<h1>Login</h1>

    				<form action="login.php" method="post" id="form">

    					<div class="field-wrap" id="errormsg" style="color: red;">

    					</div>

                        <div class="field-wrap">
    						<input name="username" type="text" required autocomplete="off" placeholder="Username" />
    					</div>

    					<div class="field-wrap">
    						<input name="password" type="password" required autocomplete="off" placeholder="Password" />
    					</div>

    					<button type="submit" class="button button-block">Login</button>

    				</form>
                    <script>
                        var request;

                        $("#form").on("submit", function(e) {
                            e.preventDefault();

                            if (request) {
                                request.abort();
                            }

                            var serializedData = $(this).serialize();

                            $(this).find("input, button").prop("disabled", true);

                            request = $.ajax({
                                url: "login.php",
                                type: "post",
                                data: serializedData
                            });

                            request.done(function (response, textStatus, jqXHR){
                                if (response.s === "true") {
                                    window.location.replace("index.php?g");
                                } else {
                                    $("#errormsg").html("Wrong username or password!");
                                }
                            });

                            request.fail(function (jqXHR, textStatus, errorThrown){
                                $("#errormsg").html("An error accured!");
                            });

                            request.always(function () {
                                $("#form").find("input, button").prop("disabled", false);
                            });
                        });
                    </script>
    			</div>
			</div>
    	</div>
    </body>
</html>