<?php

$plugins = [[
    "name" => "core",
    "folder" => "./plugins/core/",
    "frontend" => "./plugins/core/"."frontend.php",
    "backend" => "./plugins/core/"."backend.php",
    "realtime" => "./plugins/core/"."realtime.php"
]];

function validPlugin($path)
{
    $path = rtrim($path, "/")."/";
    return file_exists($path."frontend.php") && file_exists($path."backend.php") && file_exists($path."realtime.php");
}

function loadPlugins()
{
    global $plugins;
    
    foreach(scandir("./plugins/") as $plugin)
    {
        $path = "./plugins/".$plugin;
        $path = rtrim($path, "/")."/";
        if (is_dir($path) && $plugin != "." && $plugin != ".." && $plugin != "core" && validPlugin($path))
        {
            $plugins[] = [
                "name" => $plugin,
                "folder" => $path,
                "frontend" => $path."frontend.php",
                "backend" => $path."backend.php",
                "realtime" => $path."realtime.php"
            ];
        }
        if (!validPlugin($path) && $plugin != "." && $plugin != ".." && $plugin != "core")
        {
            trigger_error($plugin." is an invalid Plugin!", E_USER_ERROR);
            continue;
        }
    }
}

function includeFrontendPlugins()
{
    global $plugins;
    
    foreach($plugins as $plugin)
    {
        if ($plugin["name"] === "core" && !file_exists($plugin["frontend"]))
        {
            continue;
        }
        include $plugin["frontend"];
    }
}

function includeRealTimePlugins()
{
    global $plugins;
    
    foreach($plugins as $plugin)
    {
        if ($plugin["name"] === "core" && !file_exists($plugin["realtime"]))
        {
            continue;
        }
        include $plugin["realtime"];
    }
}

function includeBackendPlugins()
{
    global $plugins;
    
    foreach($plugins as $plugin)
    {
        if ($plugin["name"] === "core" && !file_exists($plugin["backend"]))
        {
            continue;
        }
        include $plugin["backend"];
    }
}