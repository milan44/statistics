<?php

/**
 * @author milan44
 */
final class ArchiveEntry
{
    public $date;
    public $type;
    public $value = 0;
    
    public function __construct($date, $type)
    {
        $this->setDate($date);
        $this->setType($type);
    }

    public function setDate($date)
    {
        $this->date = $date;
    }
    public function getDate()
    {
        return $this->date;
    }

    public function setType($type)
    {
        $this->type = $type;
    }
    public function getType()
    {
        return $this->type;
    }
    
    public function getValue()
    {
        return $this->value;
    }
    public function setValue($value)
    {
        $this->value = $value;
    }
    public function incrementValue()
    {
        $this->value++;
    }
}