<?php

/**
 * @author milan44
 */
final class Runner
{
    private function getGet($key)
    {
        $val = "";
        
        if (isset($_GET[$key]))
        {
            $val = $_GET[$key];
        }
        
        return base64_encode($val);
    }
    
    public function apply()
    {
        $ip = $this->getIP();
        $date = $this->getDate();
        
        $path = $this->getGet("path");
        
        $zone = $this->getGet("zone");
        
        $agent = $this->getGet("agent");
        
        $log = [
            new LogEntry($ip, $date, LogType::$HIT, $path, $zone, $agent)
        ];
        
        if (!$this->checkSession()) {
            $log[] = new LogEntry($ip, $date, LogType::$SESSION, $path, $zone, $agent);
            $this->saveSession();
        }
        if ($this->userNotLogged($ip)) {
            $log[] = new LogEntry($ip, $date, LogType::$NEWUSER, $path, $zone, $agent);
        }
        
        self::persist($log);
        
		if (isset($_GET['proxy']) && intval($_GET['proxy']) === 1)
		{
			ProxyChecker::saveProxyUser($ip);
		}
		else
		{
			ProxyChecker::saveProxyUser($ip, 0);
		}
    }
    
    public function updateRealTime($real)
    {
        $ip = $this->getIP();
        
        $real = new $real($ip);
        
        $real->update();
    }
    
    private function getIP()
    {
        return $GLOBALS["LOG"]["ip"];
    }
    
    private function getDate()
    {
        return $GLOBALS["LOG"]["date"];
    }
    
    private function checkSession()
    {
        return isset($_COOKIE["SESSION"]);
    }
    private function saveSession()
    {
        setcookie("SESSION", "logged");
    }
    private function userNotLogged($ip)
    {
        $sql = "SELECT * FROM st_log WHERE ip='".md5($ip)."' AND type=2 AND date>".strtotime("today");
        
        return !Toolbox::runExist($sql);
    }
    
    public static function persist($array)
    {
        global $connt; 
        foreach($array as $log)
        {
            Toolbox::log($log);
        }
    }
}