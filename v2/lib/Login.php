<?php

include "config.php";
include "lib/ConnectionBuilder.php";

$connt = null;

ConnectionBuilder::execute($connt, $username, $password, $database);


function checkCookie()
{
    global $connt;
    
    if (!isset($_COOKIE["login"]))
    {
        return false;
    }
    
    $login = md5($_COOKIE["login"]);
    
    $ago = time() - (60 * 60 * 24 * 7);
    
    $del = "DELETE FROM st_users WHERE time<".$ago;
    
    $connt->real_query($del);
    
    $sql = "SELECT * FROM st_users WHERE cookie = '".$login."'";
    
    $log = $connt->query($sql);
        
    if ($log->num_rows > 0)
    {
        while($row = $log->fetch_assoc())
        {
            $username = $row["username"];
            
            $cookie = getFreshCookie();
            
            $sql = "UPDATE st_users SET cookie='".md5($cookie)."', time=".time();
            
            setcookie("login", $cookie,  time()+60*60*24*8);
            
            $connt->real_query($sql);
            
            $_SESSION["login"] = true;
            $_SESSION["name"] = $username;
            
            return true;
        }
    }
    
    return false;
}

function setLoginCookie($username)
{
    global $connt;
    
    $cookie = getFreshCookie();
    
    setcookie("login", $cookie,  time()+60*60*24*8);
    
    $sql = "INSERT INTO st_users (time, username, cookie) VALUES (".time().", '".$connt->real_escape_string($username)."', '".md5($cookie)."')";
    
    $connt->real_query($sql);
}

function deleteCookie()
{
    global $connt;
    
    if (!isset($_COOKIE["login"]))
    {
        return false;
    }
    
    $login = md5($_COOKIE["login"]);
    
    $sql = "DELETE FROM st_users WHERE cookie='".$login."'";
    
    $connt->real_query($sql);
}

function getFreshCookie()
{
    return sha1(uniqid());
}