<?php

/**
 * @author milan44
 */
final class JavaScript
{

    public $data = null;
    
    public function getData()
    {
        return $this->data;
    }
    
    public function passToFE()
    {
        $ret = "<script id='vars'>";
        
        foreach ($this->data as $name => $variable)
        {
            if (is_array($variable))
            {
                $ret .= "var ".$name." = ".json_encode($variable).";";
            }
            else if ($variable === null)
            {
                $ret .= "var ".$name." = null;";
            }
            else
            {
                $ret .= "var ".$name." = ".$variable.";";
            }
            
            $ret .= "\n";
        }
        
        $ret .= "</script>";
        
        return $ret;
    }
}

