<?php

/**
 * @author milan44
 */
final class LogEntry
{
    private $date;
    private $type;
    private $ip;
    private $path;
    private $zone;
    private $agent;
    
    public function __construct($ip, $date, $type, $path = "", $zone = "", $agent = "")
    {
        $this->setIp($ip);
        $this->setDate($date);
        $this->setType($type);
        $this->setPath($path);
        $this->setZone($zone);
        $this->setAgent($agent);
    }
    
    public function getIp()
    {
        return $this->ip;
    }

    public function setIp($ip)
    {
        $this->ip = md5($ip);
    }

    public function setDate($date)
    {
        $this->date = $date;
    }
    public function getDate()
    {
        return $this->date;
    }

    public function setType($type)
    {
        $this->type = $type;
    }
    public function getType()
    {
        return $this->type;
    }

    public function setPath($path)
    {
        $this->path = $path;
    }
    public function getPath()
    {
        return $this->path;
    }
    
    public function getZone()
    {
        return $this->zone;
    }
    public function setZone($zone)
    {
        $this->zone = $zone;
    }
    
    public function getAgent()
    {
        return $this->agent;
    }
    public function setAgent($agent)
    {
        $this->agent = $agent;
    }
}

abstract class LogType
{
    public static $HIT = 0;
    public static $SESSION = 1;
    public static $NEWUSER = 2;
}