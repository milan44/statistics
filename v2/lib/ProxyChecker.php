<?php

/**
 * @author milan44
 */
class ProxyChecker
{
    public static function generateHtaccess($path, $redirectProxy, $redirectTor, $banProxies = true, $banTor = true)
    {
        global $connt;
        
        if (!$banProxies && !$banTor)
        {
            return false;
        }
        
        $htaccess = "";
        
        if ($banProxies)
        {
            $htaccess = "RewriteEngine on\n";
            $htaccess .= "\n#Banning all Proxy IPs\n";
            $sql = "SELECT ip from st_proxy_checked WHERE proxy=1 GROUP BY ip";
            
            $result = $connt->query($sql);
            
            if ($result->num_rows > 0)
            {
                while($row = $result->fetch_assoc())
                {
                    $htaccess .= "\nRewriteCond %{REMOTE_ADDR} ^".trim(preg_quote($row["ip"]))."$ [OR]";
                }
                
                $htaccess = rtrim($htaccess, " [OR]");
                
                $htaccess .= "\n\nRewriteRule ^(.*)$ ".$redirectProxy." [L]";
            }
        }
        if ($banTor)
        {
            if (empty($htaccess))
            {
                $htaccess = "RewriteEngine on\n";
            }
            
            $tor = file_get_contents(dirname(__FILE__)."/tor.txt");
            $tor = explode("\n", $tor);
            $htaccess .= "\n#Banning all Tor IPs\n";
            
            $empty = true;
            
            foreach($tor as $node)
            {
                if (!empty($node))
                {
                    $empty = false;
                    
                    $htaccess .= "\nRewriteCond %{REMOTE_ADDR} ^".trim(preg_quote($node))."$ [OR]";
                }
            }
            
            if (!$empty)
            {
                $htaccess = rtrim($htaccess, " [OR]");
                
                $htaccess .= "\n\nRewriteRule ^(.*)$ ".$redirectTor." [L]";
            }
        }
        
        $htaccess .= "\n";
        
        $info = pathinfo($path);
        $old = $info["dirname"]."/old_".$info["basename"];
        
        if (!file_exists($path)) // no htaccess file present
        {
            file_put_contents($path, $htaccess);
            file_put_contents($old, "# Do not delete this file!!!");
        }
        else if(!file_exists($old)) // htaccess file present but new one never generated
        {
            $o = file_get_contents($path);
            
            file_put_contents($old, $o);
            
            $o = file_get_contents($old);
            
            file_put_contents($path, $o."\n\n\n".$htaccess);
        }
        else // htaccess file present and new one also generated once
        {
            $o = file_get_contents($old);
            
            file_put_contents($path, $o."\n\n\n".$htaccess);
        }
        
        return true;
    }
    
    public static function updateTor()
    {
        $file = dirname(__FILE__)."/tor.txt";
        if (!file_exists($file) || filemtime($file) < (time() - (24 * 60 * 60)))
        {
            $result = self::get_web_page('https://check.torproject.org/cgi-bin/TorBulkExitList.py?ip=1.1.1.1');
            
            if ( $result['errno'] != 0 )
                return; // Fail silently
            
            if ( $result['http_code'] != 200 )
                return; // Fail silently
                
            $ips = $result['content'];
            $ips = preg_replace('/^#(.*)$/mi', '', $ips);
            
            file_put_contents($file, $ips);
        }
    }
    
    private static function get_web_page($url)
    {
        $user_agent='Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0';
        
        $options = array(
            
            CURLOPT_CUSTOMREQUEST  => "GET",        // set request type post or get
            CURLOPT_POST           => false,        // set to GET
            CURLOPT_USERAGENT      => $user_agent,  // set user agent
            CURLOPT_RETURNTRANSFER => true,         // return web page
            CURLOPT_HEADER         => false,        // don't return headers
            CURLOPT_FOLLOWLOCATION => true,         // follow redirects
            CURLOPT_ENCODING       => "",           // handle all encodings
            CURLOPT_AUTOREFERER    => true,         // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 5,            // timeout on connect
            CURLOPT_TIMEOUT        => 5,            // timeout on response
            CURLOPT_MAXREDIRS      => 10,           // stop after 10 redirects
        );
        
        $ch      = curl_init( $url );
        curl_setopt_array( $ch, $options );
        $content = curl_exec( $ch );
        $err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
        $header  = curl_getinfo( $ch );
        curl_close( $ch );
        
        $header['errno']   = $err;
        $header['errmsg']  = $errmsg;
        $header['content'] = $content;
        return $header;
    }
	
	public static function saveProxyUser($ip, $proxy = 1)
	{
		if (!self::isLogged($ip))
		{
			self::persist($ip, '', $proxy, 0);
		}
	}
    
    private static function persist($ip, $ports, $proxy, $tor)
    {
        global $connt;
        
        $ip = Toolbox::escape($ip);
        $ports = Toolbox::escape($ports);
        $proxy = intval($proxy);
        $tor = intval($tor);
        
        $sql = "INSERT INTO st_proxy_checked (time, ip, ports, proxy, tor) VALUES (";
        $sql .= time().", '".$ip."', '".$ports."', ".$proxy.", ".$tor.")";
        
        if ($connt->query($sql) === TRUE) {
            return true;
        }
        return false;
    }
    
    private static function isTor($ip)
    {
        $nodeList = dirname(__FILE__)."/tor.txt";
        
        if(exec('grep '.escapeshellarg($ip).' '.$nodeList)) {
            return true;
        }
        return false;
    }
    
    private static function isLogged($ip)
    {
        global $connt;
        
        $ip = Toolbox::escape($ip);
        
        $sql = "SELECT ports FROM st_proxy_checked WHERE ip='".$ip."'";
        
        $result = $connt->query($sql);
        
        if ($result->num_rows > 0)
        {
            while($row = $result->fetch_assoc())
            {
                return true;
            }
        }
        
        return false;
    }
}