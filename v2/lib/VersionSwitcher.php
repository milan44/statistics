<?php

/**
 * @deprecated
 * @author milan.bartky
 */
final class VersionSwitcher
{
    private $connection;
    
    public function __construct($server, $username, $password, $database)
    {
        $connection = new mysqli($server, $username, $password, $database);
        if ($connection->connect_error) {
            die("Connection failed: " . $connection->connect_error);
        }
        $this->connection = $connection;
    }
    
    public function parse($table)
    {
        $table = $this->connection->real_escape_string($table);
        $sql = "SHOW TABLES LIKE '".$table."'";
        
        $result = $this->connection->query($sql);
        if ($result->num_rows == 0) {
            die("Table does not exist");
        }
        
        $sql = "SELECT * FROM ".$table;
        
        $ret = "";
        
        $result = $this->connection->query($sql);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $ret .= $this->buildUpdateQuery($row);
            }
        } else {
            die("No Entries found");
        }
        
        return $ret;
    }
    
    public function parseAndRun($table)
    {
        $this->integretyCheck();
        
        $sql = $this->parse($table);
        $data = explode("\n", $sql);
        foreach ($data as $query) {
            if (empty($query))
            {
                continue;
            }
            $ret = $this->connection->real_query(str_replace(";", "", $query));
            if (!$ret)
            {
                die($this->connection->error);
            }
        }
    }
    
    public function close()
    {
        $this->connection->close();
    }
    
    private function integretyCheck()
    {
        $test = $this->connection->query("SELECT * FROM st_log LIMIT 1");
        
        if (empty($test))
        {
            $sql = "CREATE TABLE st_log (
	id INT(11) unsigned NOT NULL AUTO_INCREMENT,
	ip TEXT DEFAULT '',
	date INT(18) NOT NULL,
	type INT(3) NOT NULL,
                
	PRIMARY KEY (`id`)
);";
            $this->connection->query($sql);
        }
    }
    
    private function buildUpdateQuery($row)
    {
        $query = "INSERT INTO st_log (ip, date, type) VALUES (";
        
        if (!isset($row["date_str"]) || !isset($row["hits"]) || !isset($row["loads"]) || !isset($row["calls"]))
        {
            die("Unexpected table format");
        }
        
        $date = $row["date_str"];
        $date = date_create_from_format("Ymd His", $date." 000001");
        $date = $date->getTimestamp();
        
        $now = new DateTime(date("Y-m-d"));
        $now = $now->sub(new DateInterval("P7D"));
        
        if ($date < $now->getTimestamp())
        {
            return "";
        }
        
        $query .= "'', ".$date.", ";
        
        $completeQuery = "";
        
        for ($x=0;$x<intval($row["hits"]);$x++)
        {
            $completeQuery .= $query . "0);\n";
        }
        for ($x=0;$x<intval($row["loads"]);$x++)
        {
            $completeQuery .= $query . "1);\n";
        }
        for ($x=0;$x<intval($row["calls"]);$x++)
        {
            $completeQuery .= $query . "2);\n";
        }
        
        return $completeQuery;
    }
}

