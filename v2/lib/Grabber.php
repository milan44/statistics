<?php

/**
 * @author milan44
 */
final class Grabber
{
    public static function parseTemplate($vars, $template = "template.html")
    {
        $html = file_get_contents($template);

        foreach($vars as $key => $value)
        {
            $html = str_replace("{-".strtoupper($key)."-}", $value, $html);
        }
        return $html;
    }

    public static function getServerName()
    {
        $host = $_SERVER['HTTP_HOST'];

        $h = explode(".", $host);

        if ($h[0] != "www")
        {
            $host = $h[0];
        }
        else
        {
            $host = $h[1];
        }

        return $host;
    }

    public static function compile($lessSrcFile)
    {
        $lessc = new lessc;
        $less = file_get_contents($lessSrcFile);
        return $lessc->compile($less);
    }
	
	private static function endsWith($haystack, $needle)
	{
		$length = strlen($needle);
		if ($length == 0) {
			return true;
		}

		return (substr($haystack, -$length) === $needle);
	}
	
	private static function getCached()
	{
		$cache = dirname(__FILE__).'/../cache/data.json';
		
		if (!file_exists($cache))
		{
			return null;
		}
		
		$json = json_decode(file_get_contents($cache), true);
		if ($json === null || !isset($json['vars']) || !isset($json['vars']['data']) || !isset($json['vars']['hours']) || sizeof($json['vars']['data']['labels']) < 5)
		{
			return null;
		}
		
		$now = new DateTime(date("Y-m-d"));
        $now = $now->sub(new DateInterval("P3M"));
		$now->setTime(0, 0);
		$now = $now->getTimestamp();
		
		for ($x=0;$x<sizeof($json['vars']['data']['labels']);$x++)
		{
			$date = DateTime::createFromFormat('d.m.Y', $json['vars']['data']['labels'][$x]);
			
			if ($date->getTimestamp() < $now)
			{
				array_shift($json['vars']['data']['labels']);
				array_shift($json['vars']['data']['datasets'][0]['data']);
				array_shift($json['vars']['data']['datasets'][1]['data']);
				array_shift($json['vars']['data']['datasets'][2]['data']);
			}
			else
			{
				break;
			}
		}
		
		$last = null;
		for($x=sizeof($json['vars']['data']['labels'])-1;$x>=0;$x--)
		{
			
			$l = $json['vars']['data']['labels'][$x];
			if (!self::endsWith($l, 'Days'))
			{
				$last = DateTime::createFromFormat('d.m.Y (H:i:s)', $l);
				$last = $last->getTimestamp();
				break;
			}
		}
		
		array_pop($json['vars']['data']['datasets']);
		array_pop($json['vars']['data']['datasets']);
		array_pop($json['vars']['data']['datasets']);
		while($json['vars']['data']['datasets'][0]['data'][sizeof($json['vars']['data']['datasets'][0]['data'])-1] === null)
		{
			array_pop($json['vars']['data']['labels']);
			array_pop($json['vars']['data']['datasets'][0]['data']);
			array_pop($json['vars']['data']['datasets'][1]['data']);
			array_pop($json['vars']['data']['datasets'][2]['data']);
		}
		// Removing current day
		array_pop($json['vars']['data']['labels']);
		array_pop($json['vars']['data']['datasets'][0]['data']);
		array_pop($json['vars']['data']['datasets'][1]['data']);
		array_pop($json['vars']['data']['datasets'][2]['data']);
		
		// Removing yesterday (for bounce rate to be acurate)
		array_pop($json['vars']['data']['labels']);
		array_pop($json['vars']['data']['datasets'][0]['data']);
		array_pop($json['vars']['data']['datasets'][1]['data']);
		array_pop($json['vars']['data']['datasets'][2]['data']);
		
		// Removing the day before yesterday (for user loyality to be acurate)
		array_pop($json['vars']['data']['labels']);
		array_pop($json['vars']['data']['datasets'][0]['data']);
		array_pop($json['vars']['data']['datasets'][1]['data']);
		array_pop($json['vars']['data']['datasets'][2]['data']);
		
		$lastLabel = $json['vars']['data']['labels'][sizeof($json['vars']['data']['labels'])-1];
		
		$lastData = [];
		
		$label = str_replace(".", "|", $lastLabel);
		
		$lastData[] = [
			"date" => $label,
			"type" => 0,
			"value" => $json['vars']['data']['datasets'][2]['data'][sizeof($json['vars']['data']['datasets'][2]['data'])-1]
		];
		$lastData[] = [
			"date" => $label,
			"type" => 1,
			"value" => $json['vars']['data']['datasets'][1]['data'][sizeof($json['vars']['data']['datasets'][1]['data'])-1]
		];
		$lastData[] = [
			"date" => $label,
			"type" => 2,
			"value" => $json['vars']['data']['datasets'][0]['data'][sizeof($json['vars']['data']['datasets'][0]['data'])-1]
		];
		
		array_pop($json['vars']['data']['labels']);
		array_pop($json['vars']['data']['datasets'][0]['data']);
		array_pop($json['vars']['data']['datasets'][1]['data']);
		array_pop($json['vars']['data']['datasets'][2]['data']);
		
		$data = [];
		
		for($x=0;$x<sizeof($json['vars']['data']['labels']);$x++)
		{
			$label = str_replace(".", "|", $json['vars']['data']['labels'][$x]);
			$data[] = [
				"date" => $label,
				"type" => 0,
				"value" => $json['vars']['data']['datasets'][2]['data'][$x]
			];
			$data[] = [
				"date" => $label,
				"type" => 1,
				"value" => $json['vars']['data']['datasets'][1]['data'][$x]
			];
			$data[] = [
				"date" => $label,
				"type" => 2,
				"value" => $json['vars']['data']['datasets'][0]['data'][$x]
			];
		}
		
		return [
			'data' => $data,
			'lastLabel' => $lastLabel,
			'lastData' => $lastData,
			'lastUpdate' => $last
		];
	}

    public static function getLogEntries()
    {
        global $connt;
		
        $log = [];
		
		$cached = self::getCached();
		$dataAvailable = false;
		if ($cached !== null)
		{
			$dataAvailable = true;
		}


        $now = new DateTime(date("Y-m-d"));
        $now = $now->sub(new DateInterval("P3M"));
		
		$nowD = $now;
		
		if($dataAvailable)
		{
			$nowD = DateTime::createFromFormat('d.m.Y', $cached['lastLabel']);
			$nowD = $nowD->add(new DateInterval("P1D"));
		}
		
		$nowD->setTime(0, 0);

        $sql = "SELECT * FROM st_log WHERE date>".$nowD->getTimestamp()." ORDER BY date";

        $archivesql = "SELECT * FROM st_log WHERE date<".$now->getTimestamp();

        $archive = $connt->query($archivesql);

        if ($archive->num_rows > 0)
        {
            $arch = [];
            while($row = $archive->fetch_assoc())
            {
                $arch[] = $row;
            }

            $archives = self::archive($arch);

            foreach($archives as $a)
            {
                Toolbox::archive($a);
            }
        }

        $del = "DELETE FROM st_log WHERE date<".$now->getTimestamp();

        $result = $connt->query($sql);
        $aResult = $connt->query("SELECT * FROM st_archive ORDER BY date");
        $connt->real_query($del);

        if ($result->num_rows > 0)
        {
            while($row = $result->fetch_assoc())
            {
                $date = intval($row["date"]);
                $type = intval($row["type"]);
                $date = Toolbox::getDate($date);

                $entry = [
                    "date" => $date,
                    "type" => $type,
                    "ip" => $row["ip"]
                ];

                $log[] = $entry;
            }
        } else {
            $connt->close();
            die("No Entries!");
        }

        $todayStard = DateTime::createFromFormat("Ymd", date("Ymd"));
        $todayStard->setTime(0, 0);
        $todayStart = $todayStard->getTimestamp();
        $yesterdayStart = $todayStard->sub(new DateInterval('P1D'))->getTimestamp();

        $sql = "SELECT path, COUNT(*) as count FROM st_log WHERE type=0 AND date>".$yesterdayStart." GROUP BY path";
        $paths = $connt->query($sql);

        $topPaths = [
            "today" => [],
            "all" => []
        ];

        if ($paths->num_rows > 0)
        {
            while($row = $paths->fetch_assoc())
            {
                if ($row["path"] === null || $row["path"] === "")
                {
                    continue;
                }
                $topPaths["today"][] = [
                    "path" => base64_decode($row["path"]),
                    "count" => intval($row["count"])
                ];
            }
        }

        $sql = "SELECT path, COUNT(*) as count FROM st_log WHERE type=0 GROUP BY path";
        $paths = $connt->query($sql);

        if ($paths->num_rows > 0)
        {
            while($row = $paths->fetch_assoc())
            {
                if ($row["path"] === null || $row["path"] === "")
                {
                    continue;
                }
                $topPaths["all"][] = [
                    "path" => base64_decode($row["path"]),
                    "count" => intval($row["count"])
                ];
            }
        }

        $sql = "SELECT zone, COUNT(*) as count FROM st_log WHERE type=0 AND date>".$yesterdayStart." GROUP BY zone";
        $zones = $connt->query($sql);

        $topZones = [
            "today" => [],
            "all" => []
        ];

        if ($zones->num_rows > 0)
        {
            while($row = $zones->fetch_assoc())
            {
                if ($row["zone"] === null || $row["zone"] === "")
                {
                    continue;
                }
                $topZones["today"][] = [
                    "zone" => base64_decode($row["zone"]),
                    "count" => intval($row["count"])
                ];
            }
        }

        $sql = "SELECT zone, COUNT(*) as count FROM st_log WHERE type=0 GROUP BY zone";
        $zones = $connt->query($sql);

        if ($zones->num_rows > 0)
        {
            while($row = $zones->fetch_assoc())
            {
                if ($row["zone"] === null || $row["zone"] === "")
                {
                    continue;
                }
				$key = self::findInArray(base64_decode($row["zone"]), $topZones["all"]);
				
				if ($key === false)
				{
					$topZones["all"][] = [
						"zone" => base64_decode($row["zone"]),
						"count" => intval($row["count"])
					];
				}
				else
				{
					$topZones["all"][$key]["count"] += intval($row["count"]);
				}
            }
        }

        $sql = "SELECT agent, COUNT(*) as count FROM st_log WHERE type=0 AND date>".$yesterdayStart." GROUP BY agent";
        $agents = $connt->query($sql);

        $topAgents = [
            "today" => [],
            "all" => []
        ];

        if ($agents->num_rows > 0)
        {
            while($row = $agents->fetch_assoc())
            {
                if ($row["agent"] === null || $row["agent"] === "")
                {
                    continue;
                }
                $topAgents["today"][] = [
                    "agent" => base64_decode($row["agent"]),
                    "count" => intval($row["count"])
                ];
            }
        }

        $sql = "SELECT agent, COUNT(*) as count FROM st_log WHERE type=0 GROUP BY agent";
        $agents = $connt->query($sql);

        if ($agents->num_rows > 0)
        {
            while($row = $agents->fetch_assoc())
            {
                if ($row["agent"] === null || $row["agent"] === "")
                {
                    continue;
                }
				$key = self::findInArray(base64_decode($row["agent"]), $topAgents["all"]);
				
				if ($key === false)
				{
					$topAgents["all"][] = [
						"agent" => base64_decode($row["agent"]),
						"count" => intval($row["count"])
					];
				}
				else
				{
					$topAgents["all"][$key]["count"] += intval($row["count"]);
				}
            }
        }

		$maxTime = time() - (60 * 60 * 24 * 30); // 30 days

		$connt->real_query("DELETE FROM st_times where till<".$maxTime);
		$timeResult = $connt->query("SELECT * FROM st_times where NOT till=0");

		$averageTime = 0;
		$averageTimeAmount = 0;

		$times = [];

        if ($timeResult->num_rows > 0)
        {
            while($row = $timeResult->fetch_assoc())
            {
                $from = intval($row['from_time']);
                $till = intval($row['till']);
				$ip   = substr($row['ip'], 0, 32);

				$time = $till - $from;

				$date = date('Ymd', $till);

				if (!isset($times[$date]))
				{
					$times[$date] = [];
				}
				if (!isset($times[$date][$ip]))
				{
					$times[$date][$ip] = 0;
				}

				$times[$date][$ip] += $time;
            }
        }

		foreach($times as $date => $cont)
		{
			foreach($cont as $ip)
			{
				$averageTimeAmount++;
				$averageTime += $ip;
			}
		}

        $archivedEntries = [];
        if ($aResult->num_rows > 0)
        {
            while($row = $aResult->fetch_assoc())
            {
                $date = date("d.m.Y", intval($row["date"]));
                $type = intval($row["type"]);
                $value = intval($row["value"]);

                if (!isset($archivedEntries[$date]))
                {
                    $archivedEntries[$date] = [
                        "hits" => 0,
                        "sessions" => 0,
                        "user" => 0
                    ];
                }

                if ($type == 0)
                {
                    $archivedEntries[$date]["hits"] += $value;
                } else if ($type == 1)
                {
                    $archivedEntries[$date]["sessions"] += $value;
                } else if ($type == 2)
                {
                    $archivedEntries[$date]["user"] += $value;
                }
            }

            self::fillArchiveHoles($archivedEntries);

            $archivedSet = self::getEmptyChart();
            $archivedSet["datasets"][2]["label"] = "Archived Hits";
            $archivedSet["datasets"][1]["label"] = "Archived Sessions";
            $archivedSet["datasets"][0]["label"] = "Archived Visitors";
            foreach($archivedEntries as $date => $arch)
            {
                $archivedSet["labels"][] = $date;
                $archivedSet["datasets"][2]["data"][] = $arch["hits"];
                $archivedSet["datasets"][1]["data"][] = $arch["sessions"];
                $archivedSet["datasets"][0]["data"][] = $arch["user"];
            }
        }
        else
        {
            $archivedSet = null;
        }

        $bounce = [];
        self::cleanBounce($bounce, $log);

        $loyalty = self::getLoyalty($log);

        self::removeDuplicates($log);
		
		if($dataAvailable)
		{
			$log = array_merge($cached['lastData'], $log);
		}
		
        self::combine($log);

        self::cleanUp($log);

        self::fillHoles($log);
		
		if($dataAvailable)
		{
			$log = array_merge($cached['data'], $log);
		}

        $avg = self::getAVG($log);

        $max = self::getMaximum($log);
        $maxNews = self::getMaximumNewUsers($log);
        $maxHits = self::getMaximumHits($log);
        $maxSess = self::getMaximumSessions($log);

        self::prepareArrayForJS($log);
        $log["datasets"][0]["label"] = "Visitors";
        $log["datasets"][1]["label"] = "Sessions";
        $log["datasets"][2]["label"] = "Hits";
		
		
        $daylog = [
            "labels" => [],
            "datasets" => [
                [
                    "label" => "Activity per Day",
                    "data" => [],
                    "borderWidth" => 0,
                    "backgroundColor" => "rgba(24, 88, 117, 0.8)"
                ]
            ]
        ];
		for ($x=0;$x<7;$x++)
		{
			$day = self::getDayByNumber($x);
			$daylog["datasets"][0]["data"][$x] = [
				'v' => 0,
				'a' => 0
			];
			$daylog["labels"][$x] = $day;
		}
		
		for ($x=0;$x<sizeof($log['labels']);$x++)
		{
			$hit = $log['datasets'][0]['data'][$x];
			$ses = $log['datasets'][1]['data'][$x];
			$new = $log['datasets'][2]['data'][$x];
			
			$all = $hit + $ses + $new;
			
			$date = DateTime::createFromFormat('d.m.Y', $log['labels'][$x]);
			$date->setTime(0, 0);
			$day = intval($date->format('w'));
			
			$daylog["datasets"][0]["data"][$day]['v'] += $all;
			$daylog["datasets"][0]["data"][$day]['a']++;
		}
		
		for ($x=0;$x<sizeof($daylog['labels']);$x++)
		{
			$daylog["datasets"][0]["data"][$x] = round($daylog["datasets"][0]["data"][$x]['v'] / $daylog["datasets"][0]["data"][$x]['a']);
		}

        self::prepareArrayForJS($avg);
        $avg["datasets"][0]["label"] = "Average Visitors";
        $avg["datasets"][1]["label"] = "Average Sessions";
        $avg["datasets"][2]["label"] = "Average Hits";

        $log["labels"][sizeof($log["labels"])-1] .= " (".date("H:i:s").")";
        $avg["labels"][sizeof($avg["labels"])-1] .= " (".date("H:i:s").")";

        $hitsPerSec = $log["datasets"][2]["data"][sizeof($log["datasets"][2]["data"])-1];
        $sessPerSec = $log["datasets"][1]["data"][sizeof($log["datasets"][1]["data"])-1];
        $newsPerSec = $log["datasets"][0]["data"][sizeof($log["datasets"][0]["data"])-1];

        $avghitsPerSec = $avg["datasets"][2]["data"][sizeof($avg["datasets"][2]["data"])-2];
        $avgsessPerSec = $avg["datasets"][1]["data"][sizeof($avg["datasets"][1]["data"])-2];
        $avgnewsPerSec = $avg["datasets"][0]["data"][sizeof($avg["datasets"][0]["data"])-2];

        $seconds = 24.0 * 60.0 * 60.0;
        $seconds2 = (intval(date('H'))*60*60) + (intval(date('i'))*60) + intval(date('s'));

        $hitsPerSec /= $seconds2;
        $sessPerSec /= $seconds2;
        $newsPerSec /= $seconds2;
        $avghitsPerSec /= $seconds;
        $avgsessPerSec /= $seconds;
        $avgnewsPerSec /= $seconds;

        $hitRound = self::findRound($avghitsPerSec, $hitsPerSec)+1;
        $sessRound = self::findRound($avgsessPerSec, $sessPerSec)+1;
        $newsRound = self::findRound($avgnewsPerSec, $newsPerSec)+1;

		$averageTime /= $averageTimeAmount;
		$averageTime = round($averageTime);

		$h = floor($averageTime / 60 / 60);
		$averageTime -= $h * 60 * 60;
		$m = floor($averageTime / 60);
		$averageTime -= $m * 60;

		if ($h < 10)
			$h = '0' . $h;
		if ($m < 10)
			$m = '0' . $m;
		if ($averageTime < 10)
			$averageTime = '0' . $averageTime;

		$averageTime = $h . ':' . $m . ':' . $averageTime;

        return [
            "data" => $log,
            "avgdata" => $avg,
            "hours" => $daylog,
            "max" => $max,
            "maxnew" => $maxNews,
            "maxses" => $maxSess,
            "maxhit" => $maxHits,
            "bounce" => $bounce,
            "archivedEntries" => $archivedSet,
			"averageTime" => $averageTime,
            "loyalty" => round($loyalty*100, 2),
            "toppaths" => $topPaths,
            "topzones" => $topZones,
            "topagents" => $topAgents,
            "persecond" => [
                "hits" => round($hitsPerSec, $hitRound),
                "sessions" => round($sessPerSec, $sessRound),
                "users" => round($newsPerSec, $newsRound)
            ],
            "avgpersecond" => [
                "hits" => round($avghitsPerSec, $hitRound),
                "sessions" => round($avgsessPerSec, $sessRound),
                "users" => round($avgnewsPerSec, $newsRound)
            ]
        ];
    }
	
	public static function getDayByNumber($num)
	{
		switch($num) {
			case 0:
				return 'Monday';
			case 1:
				return 'Tuesday';
			case 2:
				return 'Wednesday';
			case 3:
				return 'Thursday';
			case 4:
				return 'Friday';
			case 5:
				return 'Saturday';
			case 6:
				return 'Sunday';
		}
		return 'Error';
	}
	
	public static function findInArray($what, $inWhere)
	{
		foreach($inWhere as $key => $w)
		{
			foreach($w as $wh)
			{
				if ($wh === $what)
				{
					return $key;
				}
			}
		}
		return false;
	}

    private static function findRound($num, $num2)
    {
        return 2;
    }

    private static function getLoyalty($logs)
    {
        $yesterday = Toolbox::getDate(strtotime("-2 days"));
        $now = Toolbox::getDate(strtotime("-1 days"));
        $yester = array();
        $n = array();

        foreach($logs as $log)
        {
            if ($log["date"] === $yesterday)
            {
                $yester[] = $log;
            }
            if ($log["date"] === $now)
            {
                $n[] = $log;
            }
        }

        $users_yesterday = [];
        foreach($yester as $log)
        {
            if (!in_array($log["ip"], $users_yesterday))
            {
                $users_yesterday[] = $log["ip"];
            }
        }
        $users_today = [];
        foreach($n as $log)
        {
            if (!in_array($log["ip"], $users_today))
            {
                $users_today[] = $log["ip"];
            }
        }

        $loyalty = 0;
        foreach($users_yesterday as $user)
        {
            if (in_array($user, $users_today))
            {
                $loyalty++;
            }
        }

        if (sizeof($users_yesterday) === 0 || $loyalty === 0)
        {
            $loyalty = 0;
        }
        else
        {
            $loyalty = $loyalty / sizeof($users_yesterday);
        }

        return $loyalty;
    }

    private static function archive($archive)
    {
        $newArchive = [];
        foreach($archive as $arch)
        {
            $timestamp = intval($arch["date"]);
            $date = new DateTime();
            $date->setTimestamp($timestamp);
            $date->setTime(0, 0, 0);

            $newTimestamp = $date->getTimestamp();

            if (!isset($newArchive[$newTimestamp]))
            {
                $newArchive[$newTimestamp] = [
                    0 => 0,
                    1 => 0,
                    2 => 0
                ];
            }

            $type = intval($arch["type"]);

            $newArchive[$newTimestamp][$type]++;
        }

        $archives = [];
        foreach($newArchive as $date => $arch)
        {
            $a = new ArchiveEntry($date, 0);
            $a->setValue($arch[0]);
            $archives[] = $a;

            $a = new ArchiveEntry($date, 1);
            $a->setValue($arch[1]);
            $archives[] = $a;

            $a = new ArchiveEntry($date, 2);
            $a->setValue($arch[2]);
            $archives[] = $a;
        }

        return $archives;
    }

    private static function cleanBounce(&$bounce, $log)
    {
        foreach($log as $b)
        {
            $date = $b["date"];
            $ip = $b["ip"];

            if (!isset($bounce[$date]))
            {
                $bounce[$date] = [];
                $bounce[$date][$ip] = 1;
            }
            else if (!isset($bounce[$date][$ip]))
            {
                $bounce[$date][$ip] = 1;
            }
            else if ($b["type"] === 0)
            {
                $bounce[$date][$ip]++;
            }
        }

        $nbounce = [];

        foreach($bounce as $date => $b)
        {
            $nbounce[$date] = [
                "bouncer" => 0,
                "stayer" => 0
            ];
            foreach($b as $ip)
            {
                if ($ip > 1)
                {
                    $nbounce[$date]["stayer"]++;
                }
                else
                {
                    $nbounce[$date]["bouncer"]++;
                }
            }
        }
        $bounce = $nbounce;

        $nbounce = [
            "today" => 0
        ];
        $today = date("d|m|Y", strtotime("-1 days"));

        foreach($bounce as $date => $b)
        {
            if ($b["bouncer"] === 0 || $b["stayer"] === 0)
            {
                $rate = 0;
            }
            else
            {
                $rate = $b["bouncer"] / $b["stayer"];
            }

            if ($date === $today)
            {
                $nbounce["today"] = $rate;
            }
        }
        $nbounce["today"] = ceil($nbounce["today"]*10000)/100;

        $bounce = $nbounce;
    }

    private static function getMaximum($log)
    {
        $max = 0;

        foreach ($log as $entry)
        {
            if ($entry["value"] > $max)
            {
                $max = $entry["value"];
            }
        }

        return $max;
    }
    private static function getMaximumNewUsers($log)
    {
        $max = 0;

        foreach ($log as $entry)
        {
            if ($entry["value"] > $max && $entry["type"] == 2)
            {
                $max = $entry["value"];
            }
        }

        return $max;
    }
    private static function getMaximumSessions($log)
    {
        $max = 0;

        foreach ($log as $entry)
        {
            if ($entry["value"] > $max && $entry["type"] == 1)
            {
                $max = $entry["value"];
            }
        }

        return $max;
    }
    private static function getMaximumHits($log)
    {
        $max = 0;

        foreach ($log as $entry)
        {
            if ($entry["value"] > $max && $entry["type"] == 0)
            {
                $max = $entry["value"];
            }
        }

        return $max;
    }

    private static function getAVG($log)
    {
        $nlog = [];

        $index = 0;
        $prevDate = null;

        $totalUsers = 0;
        $totalSessions = 0;
        $totalHits = 0;

        foreach ($log as $entry)
        {
            $date = $entry["date"];

            if ($date != $prevDate)
            {
                $index++;
            }

            $prevDate = $date;

            if ($entry["type"] === 2)
            {
                $totalUsers += $entry["value"];
                $nlog[] = [
                    "date" => $date,
                    "type" => 2,
                    "value" => round($totalUsers / $index, 2)
                ];
            }
            else if ($entry["type"] === 1)
            {
                $totalSessions += $entry["value"];
                $nlog[] = [
                    "date" => $date,
                    "type" => 1,
                    "value" => round($totalSessions / $index, 2)
                ];
            }
            else if ($entry["type"] === 0)
            {
                $totalHits += $entry["value"];
                $nlog[] = [
                    "date" => $date,
                    "type" => 0,
                    "value" => round($totalHits / $index, 2)
                ];
            }
        }
        return $nlog;
    }

    private static function prepareArrayForJS(&$log)
    {
        $js = self::getEmptyChart();

        $prevDate = null;

        foreach ($log as $entry)
        {
            $date = str_replace("|", ".", $entry["date"]);

            if ($date != $prevDate)
            {
                $js["labels"][] = $date;
            }

            $prevDate = $date;

            if ($entry["type"] === 2)
            {
                $js["datasets"][0]["data"][] = $entry["value"];
            }
            else if ($entry["type"] === 1)
            {
                $js["datasets"][1]["data"][] = $entry["value"];
            }
            else if ($entry["type"] === 0)
            {
                $js["datasets"][2]["data"][] = $entry["value"];
            }
        }

        $log = $js;
    }

    private static function getEmptyChart()
    {
        return [
            "labels" => [],
            "datasets" => [
                [
                    "label" => "",
                    "data" => [],
                    "borderWidth" => 1,
                    "borderColor" => "rgba(111, 167, 77, 1)",
                    "backgroundColor" => "rgba(111, 167, 77, 0.5)"
                ],
                [
                    "label" => "",
                    "data" => [],
                    "borderWidth" => 1,
                    "borderColor" => "rgba(254, 167, 77, 1)",
                    "backgroundColor" => "rgba(254, 167, 77, 0.5)"
                ],
                [
                    "label" => "",
                    "data" => [],
                    "borderWidth" => 1,
                    "borderColor" => "rgba(148, 64, 242, 1)",
                    "backgroundColor" => "rgba(148, 64, 242, 0.5)"
                ]
            ]
        ];
    }

    private static function removeDuplicates(&$log)
    {
        $nlog = [];
        $userips = array();
        foreach($log as $entry)
        {
            if (!isset($userips[$entry["date"]]))
            {
                $userips[$entry["date"]] = array();
            }

            if ($entry["type"] === 2 && in_array($entry["ip"], $userips[$entry["date"]]))
            {
                continue;
            }

            if (isset($nlog[$entry["date"]]))
            {
                if ($entry["type"] === 2)
                {
                    $nlog[$entry["date"]]["news"] += 1;
                }
                else if ($entry["type"] === 1)
                {
                    $nlog[$entry["date"]]["sess"] += 1;
                }
                else
                {
                    $nlog[$entry["date"]]["hits"] += 1;
                }
            }
            else
            {
                $nlog[$entry["date"]] = [
                    "news" => 0,
                    "sess" => 0,
                    "hits" => 0
                ];

                if ($entry["type"] === 2)
                {
                    $nlog[$entry["date"]]["news"] += 1;
                }
                else if ($entry["type"] === 1)
                {
                    $nlog[$entry["date"]]["sess"] += 1;
                }
                else
                {
                    $nlog[$entry["date"]]["hits"] += 1;
                }
            }

            if ($entry["type"] === 2)
            {
                $userips[$entry["date"]][] = $entry["ip"];
            }
        }
        $log = [];
        foreach ($nlog as $key => $day)
        {
            $log[] = [
                "date" => $key,
                "type" => 0,
                "value" => $day["hits"]
            ];
            $log[] = [
                "date" => $key,
                "type" => 1,
                "value" => $day["sess"]
            ];
            $log[] = [
                "date" => $key,
                "type" => 2,
                "value" => $day["news"]
            ];
        }
    }

    private static function combine(&$log)
    {
        $nlog = [];

        foreach ($log as $entry)
        {
            if (!isset($nlog[$entry["date"]]))
            {
                $nlog[$entry["date"]] = [];
            }
            $nlog[$entry["date"]][$entry["type"]] = $entry;
        }

        $log = $nlog;
    }

    private static function cleanUp(&$log)
    {
        $nlog = [];
        foreach ($log as $entry)
        {
            foreach($entry as $en)
            {
                $nlog[] = $en;
            }
        }
        $log = $nlog;
    }

    private static function getDateTime($dateStr, $delimiter)
    {
        $date = new DateTime();
        $date->setTime(0, 0);

        $split = explode($delimiter, $dateStr);

        $day = intval($split[0]);
        $month = intval($split[1]);
        $year = intval($split[2]);

        $date->setDate($year, $month, $day);

        return $date;
    }

    private static function fillHoles(&$log)
    {
        $nlog = [];
        $previousDate = "";

        foreach ($log as $entry)
        {
            $currentDate = $entry["date"];

            if ($previousDate === "")
            {
                $nlog[] = $entry;
                $previousDate = $currentDate;
                continue;
            }

            $current = self::getDateTime($currentDate, '|');
            $previous = self::getDateTime($previousDate, '|');

            $previous->modify('+1 day');

            for($i = $previous; $i < $current; $i->modify('+1 day'))
            {
                if ($i === null) {
                    continue;
                }

                $nlog[] = [
                    "date" => $i->format("d|m|Y"),
                    "type" => 0,
                    "value" => 0
                ];
                $nlog[] = [
                    "date" => $i->format("d|m|Y"),
                    "type" => 1,
                    "value" => 0
                ];
                $nlog[] = [
                    "date" => $i->format("d|m|Y"),
                    "type" => 2,
                    "value" => 0
                ];
            }

            $nlog[] = $entry;
            $previousDate = $currentDate;
        }

        $log = $nlog;
    }

    private static function fillArchiveHoles(&$log)
    {
        $nlog = [];
        $previousDate = "";

        foreach ($log as $currentDate => $entry)
        {
            if ($previousDate === "")
            {
                $nlog[$currentDate] = $entry;
                $previousDate = $currentDate;
                continue;
            }

            $current = self::getDateTime($currentDate, '.');
            $previous = self::getDateTime($previousDate, '.');

            $previous->modify('+1 day');

            for($i = $previous; $i < $current; $i->modify('+1 day'))
            {
                if ($i === null) {
                    continue;
                }

                $nlog[$i->format("d.m.Y")] = [
                    "hits" => 0,
                    "sessions" => 0,
                    "user" => 0
                ];
            }

            $nlog[$currentDate] = $entry;
            $previousDate = $currentDate;
        }

        $log = $nlog;
    }
}