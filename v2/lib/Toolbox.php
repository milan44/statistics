<?php

/**
 * @author milan44
 */
final class Toolbox
{
    public static function escape($str)
    {
        global $connt;
        
        return $connt->real_escape_string($str);
    }
    
    public static function runCount($sql)
    {
        global $connt;
        
        $result = $connt->query($sql);
        
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                return intval($row["count"]);
            }
        }
        
        return 0;
    }
    public static function runExist($sql)
    {
        global $connt;
        
        $result = $connt->query($sql);
        
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                return true;
            }
        }
        
        return false;
    }
    
    public static function log($log)
    {
        global $connt;
        
        $sql = self::buildSQL($log);
        
        if ($connt->real_query($sql) === TRUE) {
            return true;
        }
        return false;
    }
    
    public static function archive($log)
    {
        global $connt;
        
        $sql = self::buildSQLArchive($log);
        
        if (self::dateIsArchived($log->date, $log->type))
        {
            $sql = "UPDATE st_archive SET value = value + ".self::escape($log->value)." WHERE date=".self::escape($log->date)." AND type=".self::escape($log->type);
        }
        
        if ($connt->real_query($sql) === TRUE) {
            return true;
        }
        return false;
    }
    
    private static function dateIsArchived($timestamp, $type)
    {
        global $connt;
        $sql = "SELECT * FROM st_archive WHERE date=".self::escape($timestamp)." AND type=".self::escape($type);
        
        $result = $connt->query($sql);
        
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * @param LogEntry $log
     */
    private static function buildSQL($log)
    {
        $sql = "INSERT INTO st_log (ip, date, type, path, zone, agent) VALUES (";

        $ip = self::escape($log->getIP());
        $date = intval($log->getDate());
        $type = intval($log->getType());
        $path = self::escape($log->getPath());
        $zone = self::escape($log->getZone());
        $agent = self::escape($log->getAgent());
        
        $sql .= "'".$ip."', ".$date.", ".$type.", '".$path."', '".$zone."', '".$agent."')";
        
        return $sql;
    }
    /**
     * @param ArchiveEntry $log
     */
    private static function buildSQLArchive($log)
    {
        $sql = "INSERT INTO st_archive (date, type, value) VALUES (";

        $date = intval($log->getDate());
        $type = intval($log->getType());
        $value = intval($log->getValue());
        
        $sql .= $date.", ".$type.", ".$value.")";
        
        return $sql;
    }
    
    public static function getDate($stamp)
    {
        return date("d|m|Y", $stamp);
    }
}