<?php

/**
 * @author milan44
 */
final class RealTime
{
    private $ip;

    public function __construct($ip)
    {
        $this->ip = $ip;
    }

    public function update()
    {
        global $connt;

        $ip = Toolbox::escape($this->ip);
        $path = "";

        if (isset($_GET["path"]))
        {
            $path = $_GET["path"];
        }

        $path = base64_encode($path);

        $zone = "";

        if (isset($_GET["zone"]))
        {
            $zone = $_GET["zone"];
        }

        $zone = base64_encode($zone);

        $sql = "INSERT INTO st_realtime (time, ip, path, zone) VALUES (".time().", '".$ip."', '".$path."', '".$zone."')";

        if ($this->exists())
        {
            $sql = "UPDATE st_realtime SET time=".time()." WHERE ip='".$ip."'";
        }
		if (isset($_GET['id']))
		{
			$sql2 = "INSERT INTO st_times (ip, from_time, till) VALUES ('".$ip.md5($_GET['id'])."', ".time().", ".time().")";
			
			$ex = $this->timeExists();
			if ($ex !== false && date('Ymd', $ex) == date('Ymd'))
			{
				$sql2 = "UPDATE st_times SET till=".time()." WHERE ip='".$ip.md5($_GET['id'])."'";
			}
			
			$connt->query($sql2);
		}
		
		$connt->real_query("DELETE FROM st_times WHERE from_time<".(time() - (60 * 60))." AND till=0");
		
        if ($connt->query($sql) === TRUE) {
            return true;
        }
        return false;
    }

    public function timeExists()
    {
        global $connt;

        $ip = Toolbox::escape($this->ip);

        $sql = "SELECT * FROM st_times WHERE ip='".$ip.md5($_GET['id'])."' ORDER BY till DESC";

        $result = $connt->query($sql);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                return intval($row['till']);
            }
        }

        return false;
    }

    public function exists()
    {
        global $connt;

        $ip = Toolbox::escape($this->ip);

        $sql = "SELECT * FROM st_realtime WHERE ip='".$ip."'";

        $result = $connt->query($sql);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                return true;
            }
        }

        return false;
    }

    public static function getAmount()
    {
        global $connt;

        $sql = "SELECT zone, path FROM st_realtime";

        $result = $connt->query($sql);

        $res = [
            "amount" => 0,
            "pages" => [],
            "zones" => []
        ];

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $res["amount"]++;
                $path = base64_decode($row["path"]);
                $zone = base64_decode($row["zone"]);

                if (!isset($res["pages"][$path]))
                {
                    $res["pages"][$path] = 0;
                }
                if (!isset($res["zones"][$zone]))
                {
                    $res["zones"][$zone] = 0;
                }

                $res["pages"][$path]++;
                $res["zones"][$zone]++;
            }
        }

        return $res;
    }

    public static function removeOld()
    {
        global $connt;

        $time = time() - 30;

        $sql = "DELETE FROM st_realtime WHERE time<".$time;

        if ($connt->query($sql) === TRUE) {
            return true;
        }
        return false;
    }
}
