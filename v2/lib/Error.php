<?php

function show_msg($title, $msg, $color, $trace = "")
{
    $id = 'st_'.uniqid();
    return '<style>
.'.$id.'.error {
    font-family: Helvitica, sans-serif;
    display: inline-block;
    padding: 10px;
    border-radius: 5px;
    border: none;
    position: static;
    top: 10px;
    left: 10px;
    margin: 10px;
	max-width: 70%;
}
.'.$id.'.error h2 {
    font-size: 15px;
    margin: 0;
    margin-top: 10px;
}
.'.$id.'.error p {
    font-size: 12px;
}
.'.$id.' pre {
	white-space: pre-wrap;       /* css-3 */
	white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
	white-space: -pre-wrap;      /* Opera 4-6 */
	white-space: -o-pre-wrap;    /* Opera 7 */
	word-wrap: break-word;       /* Internet Explorer 5.5+ */
	background: #eee;
	padding: 5px;
	border-radius: 2px;
}
</style>
<div class="'.$id.' error" style="background: '.$color.'">
    <h2>'.$title.'</h2>
    <p>'.htmlentities($msg).'</p>
	<pre><code>'.$trace.'</code></pre>
</div><br>
<script>
var pres = document.querySelectorAll(".'.$id.'.error pre");
for (var x=0;x<pres.length;x++) {
    if (pres[x].innerHTML === "<code></code>") {
        pres[x].style.display = "none";
    }
}
</script>';
}

$error_handler = function ($errno, $errstr, $errline = null, $errcontext = null, $trace = "")
{
    $err = " in ".$errline." at line ".$errcontext;
    if ($errcontext === null)
    {
        $err = "";
    }
    
    $die = false;
    
    switch($errno) {
		case 1234:
			$err = show_msg("Exception".$err, $errstr, "#F66358", $trace);
            $die = true;
			break;
        case "fatal":
            $err = show_msg("Fatal Error".$err, $errstr, "#F66358");
            $die = true;
            break;
        case E_USER_ERROR:
        case E_ERROR:
            $err = show_msg("Error".$err, $errstr, "#F66358");
            $die = true;
            break;
        case E_USER_WARNING:
        case E_WARNING:
            $err = show_msg("Warning".$err, $errstr, "#FFA92B");
            break;
        case E_USER_NOTICE:
        case E_NOTICE:
            $err = show_msg("Notice".$err, $errstr, "#46A8F5");
            break;
        default:
            return;
    }
    
    if ($die)
    {
        die($err);
    }
    if ($errno === E_USER_NOTICE || $errno === E_NOTICE)
    {
        if (!defined("SHOW_E_NOTICE"))
        {
            return;
        }
    }
    echo $err;
};

set_error_handler($error_handler, E_ALL);

$fatalShutdown = function() {
    global $error_handler;
    $error = error_get_last();
    
    if ($error === null)
        return;
    
    $error_handler("fatal", $error["message"], $error["file"], $error["line"]);
};
register_shutdown_function($fatalShutdown);

function exceptionHandler($e) {
	global $error_handler;
	
	$error_handler(1234, $e->getMessage(), $e->getFile(), $e->getLine(), $e->getTraceAsString());
};

set_exception_handler('exceptionHandler');