<?php

include_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "ext/mysqldump.php";
use Ifsnop\Mysqldump\Mysqldump;

class BackupHelper
{
    public static $tables = 'st_archive st_log st_proxy_checked st_realtime st_users';
    
    public static function fullBackup()
    {
        global $username, $password, $database;
        
        $dir = dirname(__FILE__) . DIRECTORY_SEPARATOR . "../backups";
        
        self::mkdir($dir);
        
        $name = $dir . DIRECTORY_SEPARATOR . "backup_" . time() . ".sql";
        
        try {
            $dump = new Mysqldump('mysql:host=localhost;dbname='.$database, $username, $password, [
                'include-tables' => explode(' ', self::$tables)
            ]);
            $dump->start($name);
        } catch (\Exception $e) {
            echo 'mysqldump-php error: ' . $e->getMessage();
        }
    }
    
    private static function mkdir($dir)
    {
        if (!file_exists($dir))
        {
            mkdir($dir);
        }
    }
    
    public static function time_elapsed_string($datetime, $future = false) {
        $full = false;
        $now = new DateTime;
        $ago = new DateTime();
        $ago->setTimestamp($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        $str = $string ? implode(', ', $string) . ' ago' : 'just now';
        
        if ($future)
        {
            $str = 'in ' . str_replace(' ago', '', $str);
        }
        
        return $str;
    }
    
    public static function deleteOldBackups()
    {
        $dir = dirname(__FILE__) . DIRECTORY_SEPARATOR . "../backups";
        self::mkdir($dir);
        $old = strtotime("-1 week");
        
        foreach(scandir($dir) as $file)
        {
            if ($file != "." && $file != "..")
            {
                $time = str_replace("backup_", "", str_replace(".sql", "", $file));
                $file = $dir . DIRECTORY_SEPARATOR . $file;
                $time = intval($time);
                
                if ($old > $time && $time !== 0)
                {
                    unlink($file);
                }
            }
        }
    }
    
    public static function newestBackup()
    {
        $dir = dirname(__FILE__) . DIRECTORY_SEPARATOR . "../backups";
        self::mkdir($dir);
        $newest = 0;
        
        foreach(scandir($dir) as $file)
        {
            if ($file != "." && $file != "..")
            {
                $time = str_replace("backup_", "", str_replace(".sql", "", $file));
                $time = intval($time);
                
                if ($time > $newest && $time !== 0)
                {
                    $newest = $time;
                }
            }
        }
        
        return $newest;
    }
    
    public static function doBackupIfNeccesary()
    {
        $newest = self::newestBackup();
        
        if (strtotime('-1 day') > $newest)
        {
            self::deleteOldBackups();
            self::fullBackup();
        }
    }
}