<?php

/**
 * @author milan44
 */
final class TableRow
{
    private $id;
    
    private $title;
    
    private $userfield;
    
    private $sessionfield;
    
    private $hitfield;
    
    public function __construct($id, $title = "---", $userfield = "---", $sessionfield = "---", $hitfield = "---")
    {
        $this->id = $id;
        $this->title = $title;
        $this->userfield = $userfield;
        $this->sessionfield = $sessionfield;
        $this->hitfield = $hitfield;
    }
    
    public function render()
    {
        return '<tr id="'.htmlentities($this->id).'">
            <td>'.$this->title.'</td>
            <td>'.$this->userfield.'</td>
            <td>'.$this->sessionfield.'</td>
            <td>'.$this->hitfield.'</td>
        </tr>';
    }
}