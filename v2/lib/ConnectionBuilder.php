<?php

/**
 * @author milan44
 */
final class ConnectionBuilder
{
    private static function connect(&$connt, $username, $password)
    {
        $connt = new mysqli("localhost", $username, $password);

        if ($connt->connect_error)
        {
            die("Connection failed: " . $connt->connect_error);
        }
    }

    private static function connectDatabase($db)
    {
        global $connt;

        if (!$connt->select_db($db))
        {
            die("Failed to connect to Database!");
        }
    }

    private static function integretyCheck()
    {
        global $connt;

        $sql = "CREATE TABLE IF NOT EXISTS st_log (
id INT(11) unsigned NOT NULL AUTO_INCREMENT,

PRIMARY KEY (`id`)
);";
        $connt->real_query($sql);

        $sql = "ALTER TABLE st_log ADD ip TEXT DEFAULT ''";
        $connt->real_query($sql);

        $sql = "ALTER TABLE st_log ADD date INT(18) NOT NULL";
        $connt->real_query($sql);

        $sql = "ALTER TABLE st_log ADD type INT(3) NOT NULL";
        $connt->real_query($sql);

        $sql = "ALTER TABLE st_log ADD path TEXT DEFAULT ''";
        $connt->real_query($sql);

        $sql = "ALTER TABLE st_log ADD zone TEXT DEFAULT ''";
        $connt->real_query($sql);

        $sql = "ALTER TABLE st_log ADD agent TEXT DEFAULT ''";
        $connt->real_query($sql);


        $sql = "CREATE TABLE IF NOT EXISTS st_archive (
	id INT(11) unsigned NOT NULL AUTO_INCREMENT,

	PRIMARY KEY (`id`)
);";
        $connt->real_query($sql);

        $sql = "ALTER TABLE st_archive ADD date INT(18) NOT NULL";
        $connt->real_query($sql);

	    $sql = "ALTER TABLE st_archive ADD type INT(3) NOT NULL";
        $connt->real_query($sql);

        $sql = "ALTER TABLE st_archive ADD value INT(255) NOT NULL";
        $connt->real_query($sql);


        $sql = "CREATE TABLE IF NOT EXISTS st_realtime (
	id INT(11) unsigned NOT NULL AUTO_INCREMENT,

	PRIMARY KEY (`id`)
);";
        $connt->real_query($sql);

        $sql = "ALTER TABLE st_realtime ADD time INT(18) NOT NULL";
        $connt->real_query($sql);

        $sql = "ALTER TABLE st_realtime ADD ip TEXT DEFAULT ''";
        $connt->real_query($sql);

	      $sql = "ALTER TABLE st_realtime ADD path TEXT DEFAULT ''";
        $connt->real_query($sql);

	      $sql = "ALTER TABLE st_realtime ADD zone TEXT DEFAULT ''";
        $connt->real_query($sql);


        $sql = "CREATE TABLE IF NOT EXISTS st_users (
	id INT(11) unsigned NOT NULL AUTO_INCREMENT,

	PRIMARY KEY (`id`)
);";
        $connt->real_query($sql);

        $sql = "ALTER TABLE st_users ADD time INT(18) NOT NULL";
        $connt->real_query($sql);

        $sql = "ALTER TABLE st_users ADD username TEXT DEFAULT ''";
        $connt->real_query($sql);

        $sql = "ALTER TABLE st_users ADD cookie TEXT DEFAULT ''";
        $connt->real_query($sql);
        
        
        $sql = "CREATE TABLE IF NOT EXISTS st_proxy_checked (
	id INT(11) unsigned NOT NULL AUTO_INCREMENT,
            
	PRIMARY KEY (`id`)
);";
        $connt->real_query($sql);
        
        $sql = "ALTER TABLE st_proxy_checked ADD time INT(18) NOT NULL";
        $connt->real_query($sql);
        
        $sql = "ALTER TABLE st_proxy_checked ADD ip TEXT DEFAULT ''";
        $connt->real_query($sql);
        
        $sql = "ALTER TABLE st_proxy_checked ADD ports TEXT DEFAULT ''";
        $connt->real_query($sql);
        
        $sql = "ALTER TABLE st_proxy_checked ADD proxy INT(3) DEFAULT 0";
        $connt->real_query($sql);
        
        $sql = "ALTER TABLE st_proxy_checked ADD tor INT(3) DEFAULT 0";
        $connt->real_query($sql);
		
		
		$sql = "CREATE TABLE IF NOT EXISTS st_times (
	id INT(11) unsigned NOT NULL AUTO_INCREMENT,

	PRIMARY KEY (`id`)
);";
        $connt->real_query($sql);
		
		$sql = "ALTER TABLE st_times ADD ip TEXT DEFAULT ''";
        $connt->real_query($sql);

        $sql = "ALTER TABLE st_times ADD from_time INT(18) NOT NULL";
        $connt->real_query($sql);

        $sql = "ALTER TABLE st_times ADD till INT(18) NOT NULL";
        $connt->real_query($sql);
    }

    public static function execute(&$connt, $username, $password, $database)
    {
        self::connect($connt, $username, $password);
        self::connectDatabase($database);
        self::integretyCheck();
    }
}
