<?php

/**
 * @author milan44
 */
final class PluginHelper
{
    private static $vars = [];
    private static $javascriptvars = [];
    private static $javascripts = [];
    private static $less = [];

    private static $rows = [];

    public static function addTemplateVariable($key, $value)
    {
        self::$vars[$key] = $value;
    }

    public static function getVariables(&$vars)
    {
        foreach(self::$vars as $key => $value)
        {
            $vars[$key] = $value;
        }
    }

    public static function addJavaScriptVariable($key, $value)
    {
        self::$javascriptvars[$key] = $value;
    }

    public static function getJavaScriptVariables(&$javascript)
    {
        foreach(self::$javascriptvars as $key => $value)
        {
            $javascript->data[$key] = $value;
        }
    }

    public static function addLessFile($path, $filename)
    {
        self::$less[] = [
            "path" => $path,
            "filename" => $filename
        ];
    }

    public static function generateLessFiles()
    {
        foreach(self::$less as $file)
        {
            $path = rtrim($file["path"], "/")."/".$file["filename"];
            $css = Grabber::compile($path);

            file_put_contents("cache/".str_replace(".less", ".css", $file["filename"]), $css);
        }
    }

    public static function getLessFiles()
    {
        $files = '';
        foreach(self::$less as $file)
        {
            $file = "cache/".str_replace(".less", ".css", $file["filename"]);
            $files .= '<link rel="stylesheet" href="'.$file.'?'.time().'" />';
        }
        return $files;
    }

    public static function addJavaScriptFile($path, $filename)
    {
        self::$javascripts[] = [
            "path" => $path,
            "filename" => $filename
        ];
    }

    public static function getJavaScriptFiles()
    {
        $files = [];
        foreach(self::$javascripts as $file)
        {
            $files[] = "./".ltrim(rtrim($file["path"], "/"), "/")."/".$file["filename"];
        }
        return $files;
    }

    public static function addInformationRow($row)
    {
        if ($row instanceof \TableRow)
        {
            self::$rows[] = $row;
        }
        else
        {
            throw new Exception("Added Row is not of Type TableRow!");
        }
    }

    public static function getInformationRows()
    {
        return self::$rows;
    }
}
