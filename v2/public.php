<!DOCTYPE html>
<head>
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
		<title>Statistic System (v2)</title>
		<link rel="shortcut icon" type="image/png" href="./favicon.png">
		<link rel="stylesheet" href="style/initial.css" />
		<link rel="stylesheet" href="style/normalize.css" />

		<script src="js/jquery-1.11.1.min.js"></script>
		<script src="js/Chart.bundle.js"></script>
		<script src="js/chart.config.js"></script>
	</head>
	<body>
		<div id="page">
			<h1 style="text-align: center;">
				Public Statistics
			</h1>
			<div class="chartWrapper" style="position: relative">
				<canvas id="mainChart" class="chart" width="400" height="400"></canvas>
			</div>
			<div class="chartWrapper" style="position: relative">
				<canvas id="avgChart" class="chart" width="400" height="400"></canvas>
			</div>
		</div>

		<script>

			(function($) {
				function ind(e) {
					var line = null;
					$(e).on("mouseenter", function(e) {
						$(this).addClass("active");
						var left = e.pageX - $(this).offset().left;
						var height = $(this).height();
						line = document.createElement("div");
						line.style.height = height + "px";
						line.style.position = "absolute";
						line.style.left = left + "px";
						line.style.top = "0px";
						line.style.backgroundColor = "black";
						line.style.width = 1 + "px";
						line.style.pointerEvents = "none";

						$(this).parent().append(line);

						$(this).on("mouseleave", function() {
							if (line != null) {
								$(line).remove();
								$(this).removeClass("active");
							}
						});

						$(this).on("mousemove", function(e) {
							if (line != null) {
								var left = e.pageX - $(this).offset().left;
								line.style.left = left + "px";
							}
						});
					});
				};

				window.indicator = ind;
			})($);
			function getChartOptions() {
				return {
					devicePixelRatio: 2,
					tooltips: {
						mode: 'index',
						intersect: false
					},
					hover: {
						mode: 'nearest',
						intersect: true
					},
					maintainAspectRatio: false,
					scales: {
						yAxes: [{
							display: true,
							ticks: {
								beginAtZero: true
							},
							gridLines: {
								display: true,
								color: "#364363"
							}
						}],
						xAxes: [{
							gridLines: {
								display: true,
								color: "#364363"
							}
						}]
					}
				};
			}
			function fixNulls(data) {
				for (var x=0;x<data.datasets.length;x++) {
					for (var y=0;y<data.datasets[x].data.length;y++) {
						if (data.datasets[x].data[y] === 0) {
							data.datasets[x].data[y] = Number.NaN;
						}
					}
				}
				return data;
			}
			var ctx = document.getElementById("mainChart").getContext('2d');
			var ctx2 = document.getElementById("avgChart").getContext('2d');
			var options = getChartOptions();

			$.ajax({
				method: "GET",
				url: "cache/data.json",
				success: function(data) {
					var avgdata = fixNulls(data.vars.avgdata);
					data = fixNulls(data.vars.data);
					
					var vMax = 0;
					for (x=0;x<data.datasets.length;x++) {
						for (y=0;y<data.datasets[x].data.length;y++) {
							if (data.datasets[x].data[y] > vMax) {
								vMax = data.datasets[x].data[y];
							}
						}
					}
					
					options.scales.yAxes[0].ticks.max = vMax + 10;

					
					mainChart = new Chart(ctx, {
						type: 'line',
						data: data,
						options: options
					});
					indicator($('#mainChart'));
					
					mainChart2 = new Chart(ctx2, {
						type: 'line',
						data: avgdata,
						options: options
					});
					indicator($('#avgChart'));
				}
			});
		</script>
	</body>
</html>