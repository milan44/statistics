<?php
include("lib/Error.php");
define("E_NOTICE", true);

session_start();

define('TOKEN', 'aD8rE5h8YOeCog7rfAsvR6iOShGuNBMzNavezPdiRaTBUMhRRJkLc67lFwKXn2eh');

if (!file_exists(".installed"))
{
    header("Location: installer.php");
    exit;
}

define('STARTTIME', microtime(true));

if ((!isset($_SESSION["login"]) || $_SESSION["login"] != true) && (!isset($_GET['token']) || $_GET['token'] !== TOKEN))
{
    header("Location: login.php?".$_SERVER['QUERY_STRING']);
    exit;
}
if (isset($_GET["logout"]))
{
    $connt = null;

    include "lib/Login.php";

    deleteCookie();

    setcookie("login", "", time()-3600);

    session_destroy();

    $connt->close();
    header("Location: login.php");
    exit;
}

if (isset($_GET["backup"]))
{
    header("Content-type: text/plain");

    require "autoload.php";
    include "config.php";

    BackupHelper::doBackupIfNeccesary();

    $new = BackupHelper::newestBackup();
    $next = strtotime(date('Y-m-d H:i:s', $new) . "+1 day");

    if ($new === 0)
    {
        echo "Never";
    }
    else
    {
        echo '<span><span class="desc">Last Backup:</span><span class="date">' . date('d.m.Y - H:i:s', $new) . '</span>(' . BackupHelper::time_elapsed_string($new) . ")</span>";
        echo '<span><span class="desc">Next Backup:</span><span class="date">' . date('d.m.Y - H:i:s', $next) . '</span>(' . BackupHelper::time_elapsed_string($next, true) . ')</span>';
    }

    exit;
}

if (isset($_GET["full"]))
{
	if (file_exists("cache/data.json"))
	{
		unlink("cache/data.json");
	}
	header("Location: index.php?g");
    exit;
}

if (!file_exists("cache/data.json") && !isset($_GET["g"]) && !isset($_GET["new"]))
{
    header("Location: index.php?g");
    exit;
}

if (isset($_GET["new"]))
{
	
    require "autoload.php";
    require_once "lib/ext/lessc.inc.php";
    include "config.php";

    $connt = null;

    ConnectionBuilder::execute($connt, $username, $password, $database);

    ProxyChecker::updateTor();

    ProxyChecker::generateHtaccess($htacces['location'], $htacces['proxy'], $htacces['tor']);

    include "pluginloader.php";

    loadPlugins();
    includeFrontendPlugins();

    $javascript = new JavaScript();

    $entries = Grabber::getLogEntries();

    $data = $entries["data"];
	
	$data1 = $data["datasets"][0]["data"];
	$data2 = $data["datasets"][1]["data"];
	$data3 = $data["datasets"][2]["data"];
	
	array_pop($data1);
	array_pop($data2);
	array_pop($data3);

    $dat = [
    	"data3" => $data1,
    	"data2" => $data2,
    	"data" => $data3
    ];

    $_POST["silent"] = true;
    $_POST["data"] = json_encode($dat);

    include "predict.php";

    $data["datasets"][] = [
    	"backgroundColor" => "rgba(230, 230, 230, 0.25)",
    	"borderColor" => "rgba(230, 230, 230, 1)",
    	"borderWidth" => 1,
    	"data" => [],
    	"label" => "Hit Prediction"
    ];
    $data["datasets"][] = [
    	"backgroundColor" => "rgba(150, 150, 150, 0.25)",
    	"borderColor" => "rgba(150, 150, 150, 1)",
    	"borderWidth" => 1,
    	"data" => [],
    	"label" => "Session Prediction"
    ];
    $data["datasets"][] = [
    	"backgroundColor" => "rgba(255, 255, 255, 0.25)",
    	"borderColor" => "rgba(255, 255, 255, 1)",
    	"borderWidth" => 1,
    	"data" => [],
    	"label" => "Visitor Prediction"
    ];

    for ($x=0;$x<sizeof($data["labels"])-1;$x++)
    {
    	$data["datasets"][3]["data"][] = null;
    	$data["datasets"][4]["data"][] = null;
    	$data["datasets"][5]["data"][] = null;
    }
    for ($x=0;$x<sizeof($predicted["data"])-2;$x++)
    {
    	$data["labels"][] = "+".($x+1)." Days";
    }

    $data["datasets"][3]["data"][] = $predicted["data"][0];
    $data["datasets"][4]["data"][] = $predicted["data2"][0];
    $data["datasets"][5]["data"][] = $predicted["data3"][0];

    for($x=1;$x<sizeof($predicted["data"])-1;$x++)
    {
    	$data["datasets"][0]["data"][] = null;
    	$data["datasets"][1]["data"][] = null;
    	$data["datasets"][2]["data"][] = null;
        $data["datasets"][3]["data"][] = $predicted["data"][$x];
    	$data["datasets"][4]["data"][] = $predicted["data2"][$x];
    	$data["datasets"][5]["data"][] = $predicted["data3"][$x];
    }

    $entries["data"] = $data;

    $javascript->data = $entries;

    PluginHelper::getJavaScriptVariables($javascript);

    $connt->close();

    if (!file_exists('cache')) {
        mkdir('cache', 0777, true);
    }

    $rows = "";

    foreach(PluginHelper::getInformationRows() as $r)
    {
        $rows .= $r->render();
    }

    $jsFiles = PluginHelper::getJavaScriptFiles();

    $vars = [
        "vars" => $javascript->getData(),
        "name" => Grabber::getServerName(),
        "customcss" => PluginHelper::getLessFiles(),
        "customjs" => "cache/index.min.js?".time(),
        "additionalrows" => $rows,
        "timezone" => date_default_timezone_get()
    ];

    $minjs = new MatthiasMullie\Minify\JS();

    $minjs->add("./js/mobile.js");
    $minjs->add("./js/Chart.bundle.js");
    $minjs->add("./js/chart.config.js");
    $minjs->add("./js/functions.js");
    $minjs->add("./js/realtime.js");
    $minjs->add("./js/pathsAndZones.js");

    foreach($jsFiles as $js) {
        $minjs->add($js);
    }

    $minjs->add("./js/information.js");
    $minjs->add("./js/mainChart.js");
    $minjs->add("./js/averageChart.js");
    $minjs->add("./js/archiveChart.js");
    $minjs->add("./js/hourChart.js");
    $minjs->add("./js/agentChart.js");
    $minjs->add("./js/timezoneChart.js");
    $minjs->add("./js/indicator.js");

    file_put_contents("./cache/index.min.js", $minjs->minify());

    PluginHelper::generateLessFiles();
    PluginHelper::getVariables($vars);

    $login = Grabber::compile("style/login.less");
    $mobile = Grabber::compile("style/mobile.less");

    file_put_contents("cache/login.css", $login);
    file_put_contents("cache/mobile.css", $mobile);
    file_put_contents("cache/data.json", json_encode($vars));

    $_SESSION["fresh"] = true;

	$cacheTime = number_format(microtime(true) - STARTTIME, 6, '.', '');
	
    header('Content-type: application/json');
    die('{"success": 1, "time": '.$cacheTime.'}');
}

if (isset($_SESSION["fresh"]) && $_SESSION["fresh"] == true)
{
    $_SESSION["fresh"] = false;
    echo '<script>var freshCache = true;</script>';
}
	
define('ENDTIME', microtime(true));
$cacheTime = number_format(ENDTIME - STARTTIME, 6, '.', '');

echo file_get_contents("template.html");
echo '<div style="display: block;color: rgb(167, 161, 174);padding: 5px;float: right;font-size: 13px;">This Page was generated in <span id="cacheTime">'.$cacheTime.'</span> seconds.</div>';
?>