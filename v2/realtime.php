<?php
session_start();
$GLOBALS["XCLASS"] = [];

require "autoload.php";

include "config.php";

$connt = null;

ConnectionBuilder::execute($connt, $username, $password, $database);

$real = RealTime;

if (isset($GLOBALS["XCLASS"]["RealTime"]))
{
    if (!class_exists($GLOBALS["XCLASS"]["RealTime"]))
    {
        throw new Exception("RealTime XClass is not valid!");
    }

    $real = $GLOBALS["XCLASS"]["RealTime"];
}

$real::removeOld();

if(isset($_GET["get"]))
{
    header("Content-type: application/json");

    if (!isset($_SESSION["login"]) || $_SESSION["login"] != true)
    {
        die('{"success": false, "error": "unauthorized"}');
    }

    $realt = $real::getAmount();

    die('{"success": true, "amount": '.$realt["amount"].', "pages": '.json_encode($realt["pages"]).', "zones": '.json_encode($realt["zones"]).'}');
}

$GLOBALS["LOG"] = [
    "ip" => md5($_SERVER['REMOTE_ADDR'])
];

$runner = new Runner();

$runner->updateRealTime($real);

$connt->close();

?>
