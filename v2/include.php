<?php
ignore_user_abort(true);

require "autoload.php";

include "config.php";

$connt = null;

ConnectionBuilder::execute($connt, $username, $password, $database);

$runner = new Runner();

$GLOBALS["LOG"] = [
    "ip" => $_SERVER['REMOTE_ADDR'],
    "date" => time()
];

include "pluginloader.php";

loadPlugins();
includeBackendPlugins();

$runner->apply();
$connt->close();

?>