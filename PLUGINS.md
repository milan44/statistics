# Plugins

## How to build a plugin?

1. Create a new Folder inside `plugins/`
2. Create a file named `frontend.php` inside that folder
    - `frontend.php` will be included when the Cache is cleared
3. Create a file named `backend.php` inside that folder
    - `backend.php` will be included before a Hit/Session/user has been logged
3. Create a file named `realtime.php` inside that folder
    - `realtime.php` will be included when an user reports, that he is online

## $GLOBALS

When a Hit/Session/user will be logged, you can replace the ip address and date used with `$GLOBALS["LOG"]["ip"]` and `$GLOBALS["LOG"]["date"]`.

## XClass (class)

### XClass::addOverwrite($oldClass, $newClass)

You can replace a core class with one of your own.

For Example if you want to use your own RealTime Class called `MyRealTime`, you would use this function like so:

```
XClass::addOverwrite(RealTime, "MyRealTime");
```

Keep in mind, that your class needs to have all functions like the original Class.

## PluginHelper (class)

The PluginHelper provides easy to use functions for your own Plugin.

### PluginHelper::addTemplateVariable($key, $value)

This will add a variable that can be used in the template.

For Example if you write `{-MYVAR-}` inside the template and register the variable like this: `PluginHelper::addTemplateVariable("myvar", "This is a variable")`. Then uppon rendering, `{-MYVAR-}` will be replaced with `This is a variable`.

### PluginHelper::addJavaScriptVariable($key, $value)

This will add a javascript variable.

For Example if you register a variable like this: `PluginHelper::addJavaScriptVariable("myvar", 12345)`. Then uppon rendering, `var myvar = 12345;` will be added to the top of the template.

### PluginHelper::addLessFile($path, $filename)

This will add a custom LESS file. All Less files will be converted to CSS files uppon rendering.

`$path` is the path to the directory of your LESS file (relative to index.php).
`$filename` is the filename of your LESS file inside that directory (for example `mystyle.less`).

### PluginHelper::addJavaScriptFile($path, $filename)

Same thing as with `PluginHelper::addLessFile($path, $filename)` but this time a javascript file will be added to the template.

### PluginHelper::addInformationRow($row)

This function adds an additional Field in the information Table. The parameter `$row` needs to be an instance of the TableRow class.

#### TableRow($id, $title = "---", $userfield = "---", $sessionfield = "---", $itfield = "---")

The TableRow class takes 1 compulsory and 4 optional arguments.

The id will be the id of the `<tr>` element created (via that id it maybe accessed easier in javascript).  
The title will be the text shown in the title column.  
The userfield, sessionfield, hitfield will be the columns after that.